--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)

-- Started on 2019-12-27 11:43:16 -05

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13081)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3321 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 204 (class 1259 OID 34048)
-- Name: auth_group; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO lecesse;

--
-- TOC entry 203 (class 1259 OID 34046)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO lecesse;

--
-- TOC entry 3322 (class 0 OID 0)
-- Dependencies: 203
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- TOC entry 206 (class 1259 OID 34058)
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO lecesse;

--
-- TOC entry 205 (class 1259 OID 34056)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO lecesse;

--
-- TOC entry 3323 (class 0 OID 0)
-- Dependencies: 205
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- TOC entry 202 (class 1259 OID 34040)
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO lecesse;

--
-- TOC entry 201 (class 1259 OID 34038)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO lecesse;

--
-- TOC entry 3324 (class 0 OID 0)
-- Dependencies: 201
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- TOC entry 223 (class 1259 OID 42363)
-- Name: base_apartment; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_apartment (
    id integer NOT NULL,
    number integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    floor_id integer NOT NULL,
    model_id integer NOT NULL
);


ALTER TABLE public.base_apartment OWNER TO lecesse;

--
-- TOC entry 222 (class 1259 OID 42361)
-- Name: base_apartment_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_apartment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_apartment_id_seq OWNER TO lecesse;

--
-- TOC entry 3325 (class 0 OID 0)
-- Dependencies: 222
-- Name: base_apartment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_apartment_id_seq OWNED BY public.base_apartment.id;


--
-- TOC entry 225 (class 1259 OID 42371)
-- Name: base_building; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_building (
    id integer NOT NULL,
    number integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.base_building OWNER TO lecesse;

--
-- TOC entry 224 (class 1259 OID 42369)
-- Name: base_building_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_building_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_building_id_seq OWNER TO lecesse;

--
-- TOC entry 3326 (class 0 OID 0)
-- Dependencies: 224
-- Name: base_building_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_building_id_seq OWNED BY public.base_building.id;


--
-- TOC entry 221 (class 1259 OID 34205)
-- Name: base_category; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_category (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    enviroment_id integer NOT NULL,
    subcategory_id integer NOT NULL
);


ALTER TABLE public.base_category OWNER TO lecesse;

--
-- TOC entry 220 (class 1259 OID 34203)
-- Name: base_category_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_category_id_seq OWNER TO lecesse;

--
-- TOC entry 3327 (class 0 OID 0)
-- Dependencies: 220
-- Name: base_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_category_id_seq OWNED BY public.base_category.id;


--
-- TOC entry 227 (class 1259 OID 42379)
-- Name: base_contact; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_contact (
    id integer NOT NULL,
    first_name character varying(100) NOT NULL,
    last_name character varying(100) NOT NULL,
    "position" character varying(100) NOT NULL,
    phone_number character varying(100) NOT NULL,
    responsible_account character varying(100) NOT NULL,
    project_name character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.base_contact OWNER TO lecesse;

--
-- TOC entry 226 (class 1259 OID 42377)
-- Name: base_contact_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_contact_id_seq OWNER TO lecesse;

--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 226
-- Name: base_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_contact_id_seq OWNED BY public.base_contact.id;


--
-- TOC entry 208 (class 1259 OID 34092)
-- Name: base_customuser; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_customuser (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    email character varying(254) NOT NULL,
    phone_number character varying(30) NOT NULL,
    address character varying(100) NOT NULL,
    role character varying(30) NOT NULL
);


ALTER TABLE public.base_customuser OWNER TO lecesse;

--
-- TOC entry 210 (class 1259 OID 34105)
-- Name: base_customuser_groups; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_customuser_groups (
    id integer NOT NULL,
    customuser_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.base_customuser_groups OWNER TO lecesse;

--
-- TOC entry 209 (class 1259 OID 34103)
-- Name: base_customuser_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_customuser_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_customuser_groups_id_seq OWNER TO lecesse;

--
-- TOC entry 3329 (class 0 OID 0)
-- Dependencies: 209
-- Name: base_customuser_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_customuser_groups_id_seq OWNED BY public.base_customuser_groups.id;


--
-- TOC entry 207 (class 1259 OID 34090)
-- Name: base_customuser_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_customuser_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_customuser_id_seq OWNER TO lecesse;

--
-- TOC entry 3330 (class 0 OID 0)
-- Dependencies: 207
-- Name: base_customuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_customuser_id_seq OWNED BY public.base_customuser.id;


--
-- TOC entry 212 (class 1259 OID 34113)
-- Name: base_customuser_user_permissions; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_customuser_user_permissions (
    id integer NOT NULL,
    customuser_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.base_customuser_user_permissions OWNER TO lecesse;

--
-- TOC entry 211 (class 1259 OID 34111)
-- Name: base_customuser_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_customuser_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_customuser_user_permissions_id_seq OWNER TO lecesse;

--
-- TOC entry 3331 (class 0 OID 0)
-- Dependencies: 211
-- Name: base_customuser_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_customuser_user_permissions_id_seq OWNED BY public.base_customuser_user_permissions.id;


--
-- TOC entry 215 (class 1259 OID 34171)
-- Name: base_environment; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_environment (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    description character varying(150) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.base_environment OWNER TO lecesse;

--
-- TOC entry 214 (class 1259 OID 34169)
-- Name: base_environments_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_environments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_environments_id_seq OWNER TO lecesse;

--
-- TOC entry 3332 (class 0 OID 0)
-- Dependencies: 214
-- Name: base_environments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_environments_id_seq OWNED BY public.base_environment.id;


--
-- TOC entry 229 (class 1259 OID 42390)
-- Name: base_floor; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_floor (
    id integer NOT NULL,
    number integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    building_id integer NOT NULL,
    wing_id integer NOT NULL
);


ALTER TABLE public.base_floor OWNER TO lecesse;

--
-- TOC entry 228 (class 1259 OID 42388)
-- Name: base_floor_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_floor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_floor_id_seq OWNER TO lecesse;

--
-- TOC entry 3333 (class 0 OID 0)
-- Dependencies: 228
-- Name: base_floor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_floor_id_seq OWNED BY public.base_floor.id;


--
-- TOC entry 231 (class 1259 OID 42398)
-- Name: base_macroschedule; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_macroschedule (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    date_init timestamp with time zone NOT NULL,
    date_finish timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.base_macroschedule OWNER TO lecesse;

--
-- TOC entry 230 (class 1259 OID 42396)
-- Name: base_macroschedule_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_macroschedule_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_macroschedule_id_seq OWNER TO lecesse;

--
-- TOC entry 3334 (class 0 OID 0)
-- Dependencies: 230
-- Name: base_macroschedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_macroschedule_id_seq OWNED BY public.base_macroschedule.id;


--
-- TOC entry 233 (class 1259 OID 42406)
-- Name: base_material; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_material (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    description character varying(200) NOT NULL,
    image character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    category_id integer NOT NULL,
    subcategory_id integer NOT NULL,
    subcategory_two_id integer NOT NULL,
    type_measure_id integer NOT NULL
);


ALTER TABLE public.base_material OWNER TO lecesse;

--
-- TOC entry 232 (class 1259 OID 42404)
-- Name: base_material_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_material_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_material_id_seq OWNER TO lecesse;

--
-- TOC entry 3335 (class 0 OID 0)
-- Dependencies: 232
-- Name: base_material_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_material_id_seq OWNED BY public.base_material.id;


--
-- TOC entry 235 (class 1259 OID 42417)
-- Name: base_materialenvironment; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_materialenvironment (
    id integer NOT NULL,
    type_material character varying(200) NOT NULL,
    material_unity character varying(100) NOT NULL,
    unity_price integer NOT NULL,
    area_environment integer NOT NULL,
    total_price integer,
    is_proposal character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    environment_id integer NOT NULL,
    material_id integer NOT NULL
);


ALTER TABLE public.base_materialenvironment OWNER TO lecesse;

--
-- TOC entry 234 (class 1259 OID 42415)
-- Name: base_materialenvironment_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_materialenvironment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_materialenvironment_id_seq OWNER TO lecesse;

--
-- TOC entry 3336 (class 0 OID 0)
-- Dependencies: 234
-- Name: base_materialenvironment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_materialenvironment_id_seq OWNED BY public.base_materialenvironment.id;


--
-- TOC entry 247 (class 1259 OID 42476)
-- Name: base_model; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_model (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    image_flat character varying(250) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    environment_id integer NOT NULL,
    environment_material_id integer NOT NULL,
    provider_id integer NOT NULL
);


ALTER TABLE public.base_model OWNER TO lecesse;

--
-- TOC entry 246 (class 1259 OID 42474)
-- Name: base_model_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_model_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_model_id_seq OWNER TO lecesse;

--
-- TOC entry 3337 (class 0 OID 0)
-- Dependencies: 246
-- Name: base_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_model_id_seq OWNED BY public.base_model.id;


--
-- TOC entry 245 (class 1259 OID 42463)
-- Name: base_project; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_project (
    id integer NOT NULL,
    name character varying(150) NOT NULL,
    address character varying(150) NOT NULL,
    city character varying(100) NOT NULL,
    state character varying(100) NOT NULL,
    zip_code character varying(100) NOT NULL,
    owner_project character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    apartment_id integer NOT NULL,
    building_id integer NOT NULL,
    floor_id integer NOT NULL,
    macro_schedule_id integer NOT NULL,
    contact_fk_id integer NOT NULL
);


ALTER TABLE public.base_project OWNER TO lecesse;

--
-- TOC entry 244 (class 1259 OID 42461)
-- Name: base_project_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_project_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_project_id_seq OWNER TO lecesse;

--
-- TOC entry 3338 (class 0 OID 0)
-- Dependencies: 244
-- Name: base_project_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_project_id_seq OWNED BY public.base_project.id;


--
-- TOC entry 243 (class 1259 OID 42452)
-- Name: base_provider; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_provider (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    phone_number character varying(100) NOT NULL,
    email character varying(100) NOT NULL,
    address_company character varying(200) NOT NULL,
    activity character varying(100) NOT NULL,
    state character varying(100) NOT NULL,
    zip_code character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    material_id integer NOT NULL
);


ALTER TABLE public.base_provider OWNER TO lecesse;

--
-- TOC entry 242 (class 1259 OID 42450)
-- Name: base_provider_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_provider_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_provider_id_seq OWNER TO lecesse;

--
-- TOC entry 3339 (class 0 OID 0)
-- Dependencies: 242
-- Name: base_provider_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_provider_id_seq OWNED BY public.base_provider.id;


--
-- TOC entry 196 (class 1259 OID 34007)
-- Name: base_role_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_role_id_seq OWNER TO lecesse;

--
-- TOC entry 241 (class 1259 OID 42441)
-- Name: base_state; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_state (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    commentary character varying(200) NOT NULL,
    assessment character varying(150) NOT NULL,
    percentage character varying(150) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    apartment_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.base_state OWNER TO lecesse;

--
-- TOC entry 240 (class 1259 OID 42439)
-- Name: base_state_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_state_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_state_id_seq OWNER TO lecesse;

--
-- TOC entry 3340 (class 0 OID 0)
-- Dependencies: 240
-- Name: base_state_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_state_id_seq OWNED BY public.base_state.id;


--
-- TOC entry 219 (class 1259 OID 34197)
-- Name: base_subcategory; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_subcategory (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    subcategory_two_id integer NOT NULL
);


ALTER TABLE public.base_subcategory OWNER TO lecesse;

--
-- TOC entry 218 (class 1259 OID 34195)
-- Name: base_subcategory_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_subcategory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_subcategory_id_seq OWNER TO lecesse;

--
-- TOC entry 3341 (class 0 OID 0)
-- Dependencies: 218
-- Name: base_subcategory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_subcategory_id_seq OWNED BY public.base_subcategory.id;


--
-- TOC entry 217 (class 1259 OID 34189)
-- Name: base_subcategorytwo; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_subcategorytwo (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.base_subcategorytwo OWNER TO lecesse;

--
-- TOC entry 216 (class 1259 OID 34187)
-- Name: base_subcategorytwo_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_subcategorytwo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_subcategorytwo_id_seq OWNER TO lecesse;

--
-- TOC entry 3342 (class 0 OID 0)
-- Dependencies: 216
-- Name: base_subcategorytwo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_subcategorytwo_id_seq OWNED BY public.base_subcategorytwo.id;


--
-- TOC entry 237 (class 1259 OID 42425)
-- Name: base_typemeasure; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_typemeasure (
    id integer NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.base_typemeasure OWNER TO lecesse;

--
-- TOC entry 236 (class 1259 OID 42423)
-- Name: base_typemeasure_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_typemeasure_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_typemeasure_id_seq OWNER TO lecesse;

--
-- TOC entry 3343 (class 0 OID 0)
-- Dependencies: 236
-- Name: base_typemeasure_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_typemeasure_id_seq OWNED BY public.base_typemeasure.id;


--
-- TOC entry 239 (class 1259 OID 42433)
-- Name: base_wing; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.base_wing (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);


ALTER TABLE public.base_wing OWNER TO lecesse;

--
-- TOC entry 238 (class 1259 OID 42431)
-- Name: base_wing_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.base_wing_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_wing_id_seq OWNER TO lecesse;

--
-- TOC entry 3344 (class 0 OID 0)
-- Dependencies: 238
-- Name: base_wing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.base_wing_id_seq OWNED BY public.base_wing.id;


--
-- TOC entry 200 (class 1259 OID 34030)
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO lecesse;

--
-- TOC entry 199 (class 1259 OID 34028)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO lecesse;

--
-- TOC entry 3345 (class 0 OID 0)
-- Dependencies: 199
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- TOC entry 198 (class 1259 OID 34019)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO lecesse;

--
-- TOC entry 197 (class 1259 OID 34017)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: lecesse
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO lecesse;

--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 197
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lecesse
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- TOC entry 213 (class 1259 OID 34158)
-- Name: django_session; Type: TABLE; Schema: public; Owner: lecesse
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO lecesse;

--
-- TOC entry 2986 (class 2604 OID 34051)
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- TOC entry 2987 (class 2604 OID 34061)
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- TOC entry 2985 (class 2604 OID 34043)
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- TOC entry 2995 (class 2604 OID 42366)
-- Name: base_apartment id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_apartment ALTER COLUMN id SET DEFAULT nextval('public.base_apartment_id_seq'::regclass);


--
-- TOC entry 2996 (class 2604 OID 42374)
-- Name: base_building id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_building ALTER COLUMN id SET DEFAULT nextval('public.base_building_id_seq'::regclass);


--
-- TOC entry 2994 (class 2604 OID 34208)
-- Name: base_category id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_category ALTER COLUMN id SET DEFAULT nextval('public.base_category_id_seq'::regclass);


--
-- TOC entry 2997 (class 2604 OID 42382)
-- Name: base_contact id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_contact ALTER COLUMN id SET DEFAULT nextval('public.base_contact_id_seq'::regclass);


--
-- TOC entry 2988 (class 2604 OID 34095)
-- Name: base_customuser id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser ALTER COLUMN id SET DEFAULT nextval('public.base_customuser_id_seq'::regclass);


--
-- TOC entry 2989 (class 2604 OID 34108)
-- Name: base_customuser_groups id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser_groups ALTER COLUMN id SET DEFAULT nextval('public.base_customuser_groups_id_seq'::regclass);


--
-- TOC entry 2990 (class 2604 OID 34116)
-- Name: base_customuser_user_permissions id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.base_customuser_user_permissions_id_seq'::regclass);


--
-- TOC entry 2991 (class 2604 OID 34174)
-- Name: base_environment id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_environment ALTER COLUMN id SET DEFAULT nextval('public.base_environments_id_seq'::regclass);


--
-- TOC entry 2998 (class 2604 OID 42393)
-- Name: base_floor id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_floor ALTER COLUMN id SET DEFAULT nextval('public.base_floor_id_seq'::regclass);


--
-- TOC entry 2999 (class 2604 OID 42401)
-- Name: base_macroschedule id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_macroschedule ALTER COLUMN id SET DEFAULT nextval('public.base_macroschedule_id_seq'::regclass);


--
-- TOC entry 3000 (class 2604 OID 42409)
-- Name: base_material id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_material ALTER COLUMN id SET DEFAULT nextval('public.base_material_id_seq'::regclass);


--
-- TOC entry 3001 (class 2604 OID 42420)
-- Name: base_materialenvironment id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_materialenvironment ALTER COLUMN id SET DEFAULT nextval('public.base_materialenvironment_id_seq'::regclass);


--
-- TOC entry 3007 (class 2604 OID 42479)
-- Name: base_model id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_model ALTER COLUMN id SET DEFAULT nextval('public.base_model_id_seq'::regclass);


--
-- TOC entry 3006 (class 2604 OID 42466)
-- Name: base_project id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_project ALTER COLUMN id SET DEFAULT nextval('public.base_project_id_seq'::regclass);


--
-- TOC entry 3005 (class 2604 OID 42455)
-- Name: base_provider id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_provider ALTER COLUMN id SET DEFAULT nextval('public.base_provider_id_seq'::regclass);


--
-- TOC entry 3004 (class 2604 OID 42444)
-- Name: base_state id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_state ALTER COLUMN id SET DEFAULT nextval('public.base_state_id_seq'::regclass);


--
-- TOC entry 2993 (class 2604 OID 34200)
-- Name: base_subcategory id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_subcategory ALTER COLUMN id SET DEFAULT nextval('public.base_subcategory_id_seq'::regclass);


--
-- TOC entry 2992 (class 2604 OID 34192)
-- Name: base_subcategorytwo id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_subcategorytwo ALTER COLUMN id SET DEFAULT nextval('public.base_subcategorytwo_id_seq'::regclass);


--
-- TOC entry 3002 (class 2604 OID 42428)
-- Name: base_typemeasure id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_typemeasure ALTER COLUMN id SET DEFAULT nextval('public.base_typemeasure_id_seq'::regclass);


--
-- TOC entry 3003 (class 2604 OID 42436)
-- Name: base_wing id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_wing ALTER COLUMN id SET DEFAULT nextval('public.base_wing_id_seq'::regclass);


--
-- TOC entry 2984 (class 2604 OID 34033)
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- TOC entry 2983 (class 2604 OID 34022)
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- TOC entry 3270 (class 0 OID 34048)
-- Dependencies: 204
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- TOC entry 3272 (class 0 OID 34058)
-- Dependencies: 206
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- TOC entry 3268 (class 0 OID 34040)
-- Dependencies: 202
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add permission	1	add_permission
2	Can change permission	1	change_permission
3	Can delete permission	1	delete_permission
4	Can view permission	1	view_permission
5	Can add group	2	add_group
6	Can change group	2	change_group
7	Can delete group	2	delete_group
8	Can view group	2	view_group
9	Can add content type	3	add_contenttype
10	Can change content type	3	change_contenttype
11	Can delete content type	3	delete_contenttype
12	Can view content type	3	view_contenttype
13	Can add session	4	add_session
14	Can change session	4	change_session
15	Can delete session	4	delete_session
16	Can view session	4	view_session
17	Can add user	5	add_customuser
18	Can change user	5	change_customuser
19	Can delete user	5	delete_customuser
20	Can view user	5	view_customuser
21	Can add environments	6	add_environments
22	Can change environments	6	change_environments
23	Can delete environments	6	delete_environments
24	Can view environments	6	view_environments
25	Can add subcategory two	7	add_subcategorytwo
26	Can change subcategory two	7	change_subcategorytwo
27	Can delete subcategory two	7	delete_subcategorytwo
28	Can view subcategory two	7	view_subcategorytwo
29	Can add environment	6	add_environment
30	Can change environment	6	change_environment
31	Can delete environment	6	delete_environment
32	Can view environment	6	view_environment
33	Can add subcategory	8	add_subcategory
34	Can change subcategory	8	change_subcategory
35	Can delete subcategory	8	delete_subcategory
36	Can view subcategory	8	view_subcategory
37	Can add category	9	add_category
38	Can change category	9	change_category
39	Can delete category	9	delete_category
40	Can view category	9	view_category
41	Can add macro schedule	10	add_macroschedule
42	Can change macro schedule	10	change_macroschedule
43	Can delete macro schedule	10	delete_macroschedule
44	Can view macro schedule	10	view_macroschedule
45	Can add material	11	add_material
46	Can change material	11	change_material
47	Can delete material	11	delete_material
48	Can view material	11	view_material
49	Can add building	12	add_building
50	Can change building	12	change_building
51	Can delete building	12	delete_building
52	Can view building	12	view_building
53	Can add project	13	add_project
54	Can change project	13	change_project
55	Can delete project	13	delete_project
56	Can view project	13	view_project
57	Can add type measure	14	add_typemeasure
58	Can change type measure	14	change_typemeasure
59	Can delete type measure	14	delete_typemeasure
60	Can view type measure	14	view_typemeasure
61	Can add provider	15	add_provider
62	Can change provider	15	change_provider
63	Can delete provider	15	delete_provider
64	Can view provider	15	view_provider
65	Can add state	16	add_state
66	Can change state	16	change_state
67	Can delete state	16	delete_state
68	Can view state	16	view_state
69	Can add material environment	17	add_materialenvironment
70	Can change material environment	17	change_materialenvironment
71	Can delete material environment	17	delete_materialenvironment
72	Can view material environment	17	view_materialenvironment
73	Can add model	18	add_model
74	Can change model	18	change_model
75	Can delete model	18	delete_model
76	Can view model	18	view_model
77	Can add floor	19	add_floor
78	Can change floor	19	change_floor
79	Can delete floor	19	delete_floor
80	Can view floor	19	view_floor
81	Can add apartment	20	add_apartment
82	Can change apartment	20	change_apartment
83	Can delete apartment	20	delete_apartment
84	Can view apartment	20	view_apartment
85	Can add wing	21	add_wing
86	Can change wing	21	change_wing
87	Can delete wing	21	delete_wing
88	Can view wing	21	view_wing
89	Can add contact	22	add_contact
90	Can change contact	22	change_contact
91	Can delete contact	22	delete_contact
92	Can view contact	22	view_contact
\.


--
-- TOC entry 3289 (class 0 OID 42363)
-- Dependencies: 223
-- Data for Name: base_apartment; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_apartment (id, number, created_at, updated_at, floor_id, model_id) FROM stdin;
\.


--
-- TOC entry 3291 (class 0 OID 42371)
-- Dependencies: 225
-- Data for Name: base_building; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_building (id, number, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 3287 (class 0 OID 34205)
-- Dependencies: 221
-- Data for Name: base_category; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_category (id, name, created_at, updated_at, enviroment_id, subcategory_id) FROM stdin;
\.


--
-- TOC entry 3293 (class 0 OID 42379)
-- Dependencies: 227
-- Data for Name: base_contact; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_contact (id, first_name, last_name, "position", phone_number, responsible_account, project_name, created_at, updated_at) FROM stdin;
1	david	Gonzalez Arango	ewfwe	3167232106	wefwef	wefwe	2019-12-27 10:21:01.570117-05	2019-12-27 10:21:01.570149-05
2	rg	reg	reg	3167232106	regerg	ergerg	2019-12-27 10:21:13.650274-05	2019-12-27 10:21:13.650302-05
\.


--
-- TOC entry 3274 (class 0 OID 34092)
-- Dependencies: 208
-- Data for Name: base_customuser; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_customuser (id, password, last_login, is_superuser, first_name, last_name, is_staff, is_active, date_joined, email, phone_number, address, role) FROM stdin;
\.


--
-- TOC entry 3276 (class 0 OID 34105)
-- Dependencies: 210
-- Data for Name: base_customuser_groups; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_customuser_groups (id, customuser_id, group_id) FROM stdin;
\.


--
-- TOC entry 3278 (class 0 OID 34113)
-- Dependencies: 212
-- Data for Name: base_customuser_user_permissions; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_customuser_user_permissions (id, customuser_id, permission_id) FROM stdin;
\.


--
-- TOC entry 3281 (class 0 OID 34171)
-- Dependencies: 215
-- Data for Name: base_environment; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_environment (id, name, description, created_at, updated_at) FROM stdin;
12	cocina	Cocina integral con extractor de olores	2019-12-20 17:16:32.453656-05	2019-12-20 17:16:32.453695-05
\.


--
-- TOC entry 3295 (class 0 OID 42390)
-- Dependencies: 229
-- Data for Name: base_floor; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_floor (id, number, created_at, updated_at, building_id, wing_id) FROM stdin;
\.


--
-- TOC entry 3297 (class 0 OID 42398)
-- Dependencies: 231
-- Data for Name: base_macroschedule; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_macroschedule (id, name, date_init, date_finish, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 3299 (class 0 OID 42406)
-- Dependencies: 233
-- Data for Name: base_material; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_material (id, name, description, image, created_at, updated_at, category_id, subcategory_id, subcategory_two_id, type_measure_id) FROM stdin;
\.


--
-- TOC entry 3301 (class 0 OID 42417)
-- Dependencies: 235
-- Data for Name: base_materialenvironment; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_materialenvironment (id, type_material, material_unity, unity_price, area_environment, total_price, is_proposal, created_at, updated_at, environment_id, material_id) FROM stdin;
\.


--
-- TOC entry 3313 (class 0 OID 42476)
-- Dependencies: 247
-- Data for Name: base_model; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_model (id, name, image_flat, created_at, updated_at, environment_id, environment_material_id, provider_id) FROM stdin;
\.


--
-- TOC entry 3311 (class 0 OID 42463)
-- Dependencies: 245
-- Data for Name: base_project; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_project (id, name, address, city, state, zip_code, owner_project, created_at, updated_at, apartment_id, building_id, floor_id, macro_schedule_id, contact_fk_id) FROM stdin;
\.


--
-- TOC entry 3309 (class 0 OID 42452)
-- Dependencies: 243
-- Data for Name: base_provider; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_provider (id, name, phone_number, email, address_company, activity, state, zip_code, created_at, updated_at, material_id) FROM stdin;
\.


--
-- TOC entry 3307 (class 0 OID 42441)
-- Dependencies: 241
-- Data for Name: base_state; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_state (id, name, commentary, assessment, percentage, created_at, updated_at, apartment_id, user_id) FROM stdin;
\.


--
-- TOC entry 3285 (class 0 OID 34197)
-- Dependencies: 219
-- Data for Name: base_subcategory; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_subcategory (id, name, created_at, updated_at, subcategory_two_id) FROM stdin;
8	kitchen	2019-12-20 17:21:01.187931-05	2019-12-20 17:21:01.187973-05	7
\.


--
-- TOC entry 3283 (class 0 OID 34189)
-- Dependencies: 217
-- Data for Name: base_subcategorytwo; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_subcategorytwo (id, name, created_at, updated_at) FROM stdin;
7	kitchen cabinet	2019-12-20 17:20:11.619818-05	2019-12-20 17:20:28.331435-05
\.


--
-- TOC entry 3303 (class 0 OID 42425)
-- Dependencies: 237
-- Data for Name: base_typemeasure; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_typemeasure (id, name) FROM stdin;
4	uykfuk
\.


--
-- TOC entry 3305 (class 0 OID 42433)
-- Dependencies: 239
-- Data for Name: base_wing; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.base_wing (id, name, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 3266 (class 0 OID 34030)
-- Dependencies: 200
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	auth	permission
2	auth	group
3	contenttypes	contenttype
4	sessions	session
5	base	customuser
6	base	environment
7	base	subcategorytwo
8	base	subcategory
9	base	category
10	base	macroschedule
11	base	material
12	base	building
13	base	project
14	base	typemeasure
15	base	provider
16	base	state
17	base	materialenvironment
18	base	model
19	base	floor
20	base	apartment
21	base	wing
22	base	contact
\.


--
-- TOC entry 3264 (class 0 OID 34019)
-- Dependencies: 198
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2019-12-18 14:47:11.269666-05
2	contenttypes	0002_remove_content_type_name	2019-12-18 14:47:11.281048-05
3	auth	0001_initial	2019-12-18 14:47:11.314227-05
4	auth	0002_alter_permission_name_max_length	2019-12-18 14:47:11.349297-05
5	auth	0003_alter_user_email_max_length	2019-12-18 14:47:11.357773-05
6	auth	0004_alter_user_username_opts	2019-12-18 14:47:11.365582-05
7	auth	0005_alter_user_last_login_null	2019-12-18 14:47:11.373108-05
8	auth	0006_require_contenttypes_0002	2019-12-18 14:47:11.376967-05
9	auth	0007_alter_validators_add_error_messages	2019-12-18 14:47:11.382455-05
10	auth	0008_alter_user_username_max_length	2019-12-18 14:47:11.390384-05
11	auth	0009_alter_user_last_name_max_length	2019-12-18 14:47:11.399083-05
12	auth	0010_alter_group_name_max_length	2019-12-18 14:47:11.408199-05
13	auth	0011_update_proxy_permissions	2019-12-18 14:47:11.415213-05
14	base	0001_initial	2019-12-18 14:47:11.451171-05
15	base	0002_customuser_role	2019-12-18 14:47:11.509258-05
16	sessions	0001_initial	2019-12-18 14:47:11.521759-05
17	base	0003_auto_20191218_1947	2019-12-18 14:48:03.332611-05
18	base	0004_auto_20191219_0525	2019-12-19 08:33:15.553741-05
19	base	0005_auto_20191219_1346	2019-12-19 08:46:29.896786-05
20	base	0006_auto_20191219_1452	2019-12-19 09:52:43.608617-05
21	base	0007_auto_20191226_1336	2019-12-26 08:37:04.135505-05
22	base	0008_auto_20191226_1339	2019-12-26 08:39:58.000959-05
23	base	0009_auto_20191226_1613	2019-12-26 11:14:04.666863-05
24	base	0009_auto_20191226_1707	2019-12-26 12:07:28.611611-05
25	base	0010_auto_20191226_1842	2019-12-26 13:42:14.634057-05
\.


--
-- TOC entry 3279 (class 0 OID 34158)
-- Dependencies: 213
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: lecesse
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 203
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 205
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- TOC entry 3349 (class 0 OID 0)
-- Dependencies: 201
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 92, true);


--
-- TOC entry 3350 (class 0 OID 0)
-- Dependencies: 222
-- Name: base_apartment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_apartment_id_seq', 1, false);


--
-- TOC entry 3351 (class 0 OID 0)
-- Dependencies: 224
-- Name: base_building_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_building_id_seq', 1, false);


--
-- TOC entry 3352 (class 0 OID 0)
-- Dependencies: 220
-- Name: base_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_category_id_seq', 7, true);


--
-- TOC entry 3353 (class 0 OID 0)
-- Dependencies: 226
-- Name: base_contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_contact_id_seq', 2, true);


--
-- TOC entry 3354 (class 0 OID 0)
-- Dependencies: 209
-- Name: base_customuser_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_customuser_groups_id_seq', 1, false);


--
-- TOC entry 3355 (class 0 OID 0)
-- Dependencies: 207
-- Name: base_customuser_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_customuser_id_seq', 7, true);


--
-- TOC entry 3356 (class 0 OID 0)
-- Dependencies: 211
-- Name: base_customuser_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_customuser_user_permissions_id_seq', 1, false);


--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 214
-- Name: base_environments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_environments_id_seq', 12, true);


--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 228
-- Name: base_floor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_floor_id_seq', 1, false);


--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 230
-- Name: base_macroschedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_macroschedule_id_seq', 1, false);


--
-- TOC entry 3360 (class 0 OID 0)
-- Dependencies: 232
-- Name: base_material_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_material_id_seq', 7, true);


--
-- TOC entry 3361 (class 0 OID 0)
-- Dependencies: 234
-- Name: base_materialenvironment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_materialenvironment_id_seq', 3, true);


--
-- TOC entry 3362 (class 0 OID 0)
-- Dependencies: 246
-- Name: base_model_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_model_id_seq', 1, false);


--
-- TOC entry 3363 (class 0 OID 0)
-- Dependencies: 244
-- Name: base_project_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_project_id_seq', 1, false);


--
-- TOC entry 3364 (class 0 OID 0)
-- Dependencies: 242
-- Name: base_provider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_provider_id_seq', 1, false);


--
-- TOC entry 3365 (class 0 OID 0)
-- Dependencies: 196
-- Name: base_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_role_id_seq', 1, false);


--
-- TOC entry 3366 (class 0 OID 0)
-- Dependencies: 240
-- Name: base_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_state_id_seq', 1, false);


--
-- TOC entry 3367 (class 0 OID 0)
-- Dependencies: 218
-- Name: base_subcategory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_subcategory_id_seq', 8, true);


--
-- TOC entry 3368 (class 0 OID 0)
-- Dependencies: 216
-- Name: base_subcategorytwo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_subcategorytwo_id_seq', 7, true);


--
-- TOC entry 3369 (class 0 OID 0)
-- Dependencies: 236
-- Name: base_typemeasure_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_typemeasure_id_seq', 4, true);


--
-- TOC entry 3370 (class 0 OID 0)
-- Dependencies: 238
-- Name: base_wing_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.base_wing_id_seq', 1, false);


--
-- TOC entry 3371 (class 0 OID 0)
-- Dependencies: 199
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 22, true);


--
-- TOC entry 3372 (class 0 OID 0)
-- Dependencies: 197
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lecesse
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 25, true);


--
-- TOC entry 3021 (class 2606 OID 34088)
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 3026 (class 2606 OID 34084)
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- TOC entry 3029 (class 2606 OID 34063)
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3023 (class 2606 OID 34053)
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 3016 (class 2606 OID 34070)
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- TOC entry 3018 (class 2606 OID 34045)
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 3065 (class 2606 OID 42368)
-- Name: base_apartment base_apartment_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_apartment
    ADD CONSTRAINT base_apartment_pkey PRIMARY KEY (id);


--
-- TOC entry 3067 (class 2606 OID 42376)
-- Name: base_building base_building_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_building
    ADD CONSTRAINT base_building_pkey PRIMARY KEY (id);


--
-- TOC entry 3060 (class 2606 OID 34210)
-- Name: base_category base_category_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_category
    ADD CONSTRAINT base_category_pkey PRIMARY KEY (id);


--
-- TOC entry 3069 (class 2606 OID 42387)
-- Name: base_contact base_contact_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_contact
    ADD CONSTRAINT base_contact_pkey PRIMARY KEY (id);


--
-- TOC entry 3032 (class 2606 OID 34102)
-- Name: base_customuser base_customuser_email_key; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser
    ADD CONSTRAINT base_customuser_email_key UNIQUE (email);


--
-- TOC entry 3037 (class 2606 OID 34131)
-- Name: base_customuser_groups base_customuser_groups_customuser_id_group_id_d4e28d0b_uniq; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser_groups
    ADD CONSTRAINT base_customuser_groups_customuser_id_group_id_d4e28d0b_uniq UNIQUE (customuser_id, group_id);


--
-- TOC entry 3040 (class 2606 OID 34110)
-- Name: base_customuser_groups base_customuser_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser_groups
    ADD CONSTRAINT base_customuser_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 3034 (class 2606 OID 34100)
-- Name: base_customuser base_customuser_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser
    ADD CONSTRAINT base_customuser_pkey PRIMARY KEY (id);


--
-- TOC entry 3042 (class 2606 OID 34145)
-- Name: base_customuser_user_permissions base_customuser_user_per_customuser_id_permission_90414b11_uniq; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser_user_permissions
    ADD CONSTRAINT base_customuser_user_per_customuser_id_permission_90414b11_uniq UNIQUE (customuser_id, permission_id);


--
-- TOC entry 3046 (class 2606 OID 34118)
-- Name: base_customuser_user_permissions base_customuser_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser_user_permissions
    ADD CONSTRAINT base_customuser_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3052 (class 2606 OID 34176)
-- Name: base_environment base_environments_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_environment
    ADD CONSTRAINT base_environments_pkey PRIMARY KEY (id);


--
-- TOC entry 3072 (class 2606 OID 42395)
-- Name: base_floor base_floor_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_floor
    ADD CONSTRAINT base_floor_pkey PRIMARY KEY (id);


--
-- TOC entry 3075 (class 2606 OID 42403)
-- Name: base_macroschedule base_macroschedule_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_macroschedule
    ADD CONSTRAINT base_macroschedule_pkey PRIMARY KEY (id);


--
-- TOC entry 3078 (class 2606 OID 42414)
-- Name: base_material base_material_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_material
    ADD CONSTRAINT base_material_pkey PRIMARY KEY (id);


--
-- TOC entry 3085 (class 2606 OID 42422)
-- Name: base_materialenvironment base_materialenvironment_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_materialenvironment
    ADD CONSTRAINT base_materialenvironment_pkey PRIMARY KEY (id);


--
-- TOC entry 3108 (class 2606 OID 42481)
-- Name: base_model base_model_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_model
    ADD CONSTRAINT base_model_pkey PRIMARY KEY (id);


--
-- TOC entry 3102 (class 2606 OID 42473)
-- Name: base_project base_project_macro_schedule_id_key; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_project
    ADD CONSTRAINT base_project_macro_schedule_id_key UNIQUE (macro_schedule_id);


--
-- TOC entry 3104 (class 2606 OID 42471)
-- Name: base_project base_project_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_project
    ADD CONSTRAINT base_project_pkey PRIMARY KEY (id);


--
-- TOC entry 3096 (class 2606 OID 42460)
-- Name: base_provider base_provider_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_provider
    ADD CONSTRAINT base_provider_pkey PRIMARY KEY (id);


--
-- TOC entry 3092 (class 2606 OID 42449)
-- Name: base_state base_state_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_state
    ADD CONSTRAINT base_state_pkey PRIMARY KEY (id);


--
-- TOC entry 3056 (class 2606 OID 34202)
-- Name: base_subcategory base_subcategory_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_subcategory
    ADD CONSTRAINT base_subcategory_pkey PRIMARY KEY (id);


--
-- TOC entry 3054 (class 2606 OID 34194)
-- Name: base_subcategorytwo base_subcategorytwo_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_subcategorytwo
    ADD CONSTRAINT base_subcategorytwo_pkey PRIMARY KEY (id);


--
-- TOC entry 3087 (class 2606 OID 42430)
-- Name: base_typemeasure base_typemeasure_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_typemeasure
    ADD CONSTRAINT base_typemeasure_pkey PRIMARY KEY (id);


--
-- TOC entry 3089 (class 2606 OID 42438)
-- Name: base_wing base_wing_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_wing
    ADD CONSTRAINT base_wing_pkey PRIMARY KEY (id);


--
-- TOC entry 3011 (class 2606 OID 34037)
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- TOC entry 3013 (class 2606 OID 34035)
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3009 (class 2606 OID 34027)
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 3049 (class 2606 OID 34165)
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 3019 (class 1259 OID 34089)
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 3024 (class 1259 OID 34085)
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- TOC entry 3027 (class 1259 OID 34086)
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- TOC entry 3014 (class 1259 OID 34071)
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- TOC entry 3062 (class 1259 OID 42590)
-- Name: base_apartment_floor_id_804170a8; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_apartment_floor_id_804170a8 ON public.base_apartment USING btree (floor_id);


--
-- TOC entry 3063 (class 1259 OID 42596)
-- Name: base_apartment_model_id_5e1438a1; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_apartment_model_id_5e1438a1 ON public.base_apartment USING btree (model_id);


--
-- TOC entry 3058 (class 1259 OID 34227)
-- Name: base_category_enviroment_id_0c82f603; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_category_enviroment_id_0c82f603 ON public.base_category USING btree (enviroment_id);


--
-- TOC entry 3061 (class 1259 OID 34228)
-- Name: base_category_subcategory_id_0aab6d4c; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_category_subcategory_id_0aab6d4c ON public.base_category USING btree (subcategory_id);


--
-- TOC entry 3030 (class 1259 OID 34119)
-- Name: base_customuser_email_92a56e02_like; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_customuser_email_92a56e02_like ON public.base_customuser USING btree (email varchar_pattern_ops);


--
-- TOC entry 3035 (class 1259 OID 34132)
-- Name: base_customuser_groups_customuser_id_04d7166b; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_customuser_groups_customuser_id_04d7166b ON public.base_customuser_groups USING btree (customuser_id);


--
-- TOC entry 3038 (class 1259 OID 34133)
-- Name: base_customuser_groups_group_id_d1822349; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_customuser_groups_group_id_d1822349 ON public.base_customuser_groups USING btree (group_id);


--
-- TOC entry 3043 (class 1259 OID 34146)
-- Name: base_customuser_user_permissions_customuser_id_4f46199a; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_customuser_user_permissions_customuser_id_4f46199a ON public.base_customuser_user_permissions USING btree (customuser_id);


--
-- TOC entry 3044 (class 1259 OID 34147)
-- Name: base_customuser_user_permissions_permission_id_e62eddd3; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_customuser_user_permissions_permission_id_e62eddd3 ON public.base_customuser_user_permissions USING btree (permission_id);


--
-- TOC entry 3070 (class 1259 OID 42489)
-- Name: base_floor_building_id_f76f7c39; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_floor_building_id_f76f7c39 ON public.base_floor USING btree (building_id);


--
-- TOC entry 3073 (class 1259 OID 42584)
-- Name: base_floor_wing_id_3192bf12; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_floor_wing_id_3192bf12 ON public.base_floor USING btree (wing_id);


--
-- TOC entry 3076 (class 1259 OID 42505)
-- Name: base_material_category_id_8e785cc4; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_material_category_id_8e785cc4 ON public.base_material USING btree (category_id);


--
-- TOC entry 3079 (class 1259 OID 42506)
-- Name: base_material_subcategory_id_39674182; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_material_subcategory_id_39674182 ON public.base_material USING btree (subcategory_id);


--
-- TOC entry 3080 (class 1259 OID 42507)
-- Name: base_material_subcategory_two_id_3b8688e7; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_material_subcategory_two_id_3b8688e7 ON public.base_material USING btree (subcategory_two_id);


--
-- TOC entry 3081 (class 1259 OID 42617)
-- Name: base_material_type_measure_id_a54489fe; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_material_type_measure_id_a54489fe ON public.base_material USING btree (type_measure_id);


--
-- TOC entry 3082 (class 1259 OID 42518)
-- Name: base_materialenvironment_environment_id_805eed95; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_materialenvironment_environment_id_805eed95 ON public.base_materialenvironment USING btree (environment_id);


--
-- TOC entry 3083 (class 1259 OID 42519)
-- Name: base_materialenvironment_material_id_19bb0858; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_materialenvironment_material_id_19bb0858 ON public.base_materialenvironment USING btree (material_id);


--
-- TOC entry 3105 (class 1259 OID 42576)
-- Name: base_model_environment_id_a2b547ec; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_model_environment_id_a2b547ec ON public.base_model USING btree (environment_id);


--
-- TOC entry 3106 (class 1259 OID 42577)
-- Name: base_model_environment_material_id_917decbe; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_model_environment_material_id_917decbe ON public.base_model USING btree (environment_material_id);


--
-- TOC entry 3109 (class 1259 OID 42578)
-- Name: base_model_provider_id_4c3757a8; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_model_provider_id_4c3757a8 ON public.base_model USING btree (provider_id);


--
-- TOC entry 3097 (class 1259 OID 42558)
-- Name: base_project_apartment_id_9d2f6235; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_project_apartment_id_9d2f6235 ON public.base_project USING btree (apartment_id);


--
-- TOC entry 3098 (class 1259 OID 42559)
-- Name: base_project_building_id_3d003c4c; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_project_building_id_3d003c4c ON public.base_project USING btree (building_id);


--
-- TOC entry 3099 (class 1259 OID 42603)
-- Name: base_project_contact_fk_id_c74d5bf0; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_project_contact_fk_id_c74d5bf0 ON public.base_project USING btree (contact_fk_id);


--
-- TOC entry 3100 (class 1259 OID 42560)
-- Name: base_project_floor_id_766c641a; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_project_floor_id_766c641a ON public.base_project USING btree (floor_id);


--
-- TOC entry 3094 (class 1259 OID 42537)
-- Name: base_provider_material_id_7d6bda5c; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_provider_material_id_7d6bda5c ON public.base_provider USING btree (material_id);


--
-- TOC entry 3090 (class 1259 OID 42530)
-- Name: base_state_apartment_id_d0989ff4; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_state_apartment_id_d0989ff4 ON public.base_state USING btree (apartment_id);


--
-- TOC entry 3093 (class 1259 OID 42531)
-- Name: base_state_user_id_58f411f7; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_state_user_id_58f411f7 ON public.base_state USING btree (user_id);


--
-- TOC entry 3057 (class 1259 OID 34216)
-- Name: base_subcategory_subcategory_two_id_319517c6; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX base_subcategory_subcategory_two_id_319517c6 ON public.base_subcategory USING btree (subcategory_two_id);


--
-- TOC entry 3047 (class 1259 OID 34167)
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- TOC entry 3050 (class 1259 OID 34166)
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: lecesse
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 3112 (class 2606 OID 34078)
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3111 (class 2606 OID 34073)
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3110 (class 2606 OID 34064)
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3120 (class 2606 OID 42591)
-- Name: base_apartment base_apartment_floor_id_804170a8_fk_base_floor_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_apartment
    ADD CONSTRAINT base_apartment_floor_id_804170a8_fk_base_floor_id FOREIGN KEY (floor_id) REFERENCES public.base_floor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3121 (class 2606 OID 42597)
-- Name: base_apartment base_apartment_model_id_5e1438a1_fk_base_model_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_apartment
    ADD CONSTRAINT base_apartment_model_id_5e1438a1_fk_base_model_id FOREIGN KEY (model_id) REFERENCES public.base_model(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3118 (class 2606 OID 34217)
-- Name: base_category base_category_enviroment_id_0c82f603_fk_base_environment_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_category
    ADD CONSTRAINT base_category_enviroment_id_0c82f603_fk_base_environment_id FOREIGN KEY (enviroment_id) REFERENCES public.base_environment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3119 (class 2606 OID 34222)
-- Name: base_category base_category_subcategory_id_0aab6d4c_fk_base_subcategorytwo_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_category
    ADD CONSTRAINT base_category_subcategory_id_0aab6d4c_fk_base_subcategorytwo_id FOREIGN KEY (subcategory_id) REFERENCES public.base_subcategorytwo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3113 (class 2606 OID 34120)
-- Name: base_customuser_groups base_customuser_grou_customuser_id_04d7166b_fk_base_cust; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser_groups
    ADD CONSTRAINT base_customuser_grou_customuser_id_04d7166b_fk_base_cust FOREIGN KEY (customuser_id) REFERENCES public.base_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3114 (class 2606 OID 34125)
-- Name: base_customuser_groups base_customuser_groups_group_id_d1822349_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser_groups
    ADD CONSTRAINT base_customuser_groups_group_id_d1822349_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3115 (class 2606 OID 34134)
-- Name: base_customuser_user_permissions base_customuser_user_customuser_id_4f46199a_fk_base_cust; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser_user_permissions
    ADD CONSTRAINT base_customuser_user_customuser_id_4f46199a_fk_base_cust FOREIGN KEY (customuser_id) REFERENCES public.base_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3116 (class 2606 OID 34139)
-- Name: base_customuser_user_permissions base_customuser_user_permission_id_e62eddd3_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_customuser_user_permissions
    ADD CONSTRAINT base_customuser_user_permission_id_e62eddd3_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3122 (class 2606 OID 42484)
-- Name: base_floor base_floor_building_id_f76f7c39_fk_base_building_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_floor
    ADD CONSTRAINT base_floor_building_id_f76f7c39_fk_base_building_id FOREIGN KEY (building_id) REFERENCES public.base_building(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3123 (class 2606 OID 42585)
-- Name: base_floor base_floor_wing_id_3192bf12_fk_base_wing_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_floor
    ADD CONSTRAINT base_floor_wing_id_3192bf12_fk_base_wing_id FOREIGN KEY (wing_id) REFERENCES public.base_wing(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3124 (class 2606 OID 42490)
-- Name: base_material base_material_category_id_8e785cc4_fk_base_category_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_material
    ADD CONSTRAINT base_material_category_id_8e785cc4_fk_base_category_id FOREIGN KEY (category_id) REFERENCES public.base_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3125 (class 2606 OID 42495)
-- Name: base_material base_material_subcategory_id_39674182_fk_base_subcategory_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_material
    ADD CONSTRAINT base_material_subcategory_id_39674182_fk_base_subcategory_id FOREIGN KEY (subcategory_id) REFERENCES public.base_subcategory(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3126 (class 2606 OID 42500)
-- Name: base_material base_material_subcategory_two_id_3b8688e7_fk_base_subc; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_material
    ADD CONSTRAINT base_material_subcategory_two_id_3b8688e7_fk_base_subc FOREIGN KEY (subcategory_two_id) REFERENCES public.base_subcategorytwo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3127 (class 2606 OID 42618)
-- Name: base_material base_material_type_measure_id_a54489fe_fk_base_typemeasure_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_material
    ADD CONSTRAINT base_material_type_measure_id_a54489fe_fk_base_typemeasure_id FOREIGN KEY (type_measure_id) REFERENCES public.base_typemeasure(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3128 (class 2606 OID 42508)
-- Name: base_materialenvironment base_materialenviron_environment_id_805eed95_fk_base_envi; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_materialenvironment
    ADD CONSTRAINT base_materialenviron_environment_id_805eed95_fk_base_envi FOREIGN KEY (environment_id) REFERENCES public.base_environment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3129 (class 2606 OID 42513)
-- Name: base_materialenvironment base_materialenviron_material_id_19bb0858_fk_base_mate; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_materialenvironment
    ADD CONSTRAINT base_materialenviron_material_id_19bb0858_fk_base_mate FOREIGN KEY (material_id) REFERENCES public.base_material(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3138 (class 2606 OID 42561)
-- Name: base_model base_model_environment_id_a2b547ec_fk_base_environment_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_model
    ADD CONSTRAINT base_model_environment_id_a2b547ec_fk_base_environment_id FOREIGN KEY (environment_id) REFERENCES public.base_environment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3139 (class 2606 OID 42566)
-- Name: base_model base_model_environment_material_917decbe_fk_base_mate; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_model
    ADD CONSTRAINT base_model_environment_material_917decbe_fk_base_mate FOREIGN KEY (environment_material_id) REFERENCES public.base_materialenvironment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3140 (class 2606 OID 42571)
-- Name: base_model base_model_provider_id_4c3757a8_fk_base_provider_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_model
    ADD CONSTRAINT base_model_provider_id_4c3757a8_fk_base_provider_id FOREIGN KEY (provider_id) REFERENCES public.base_provider(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3133 (class 2606 OID 42538)
-- Name: base_project base_project_apartment_id_9d2f6235_fk_base_apartment_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_project
    ADD CONSTRAINT base_project_apartment_id_9d2f6235_fk_base_apartment_id FOREIGN KEY (apartment_id) REFERENCES public.base_apartment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3134 (class 2606 OID 42543)
-- Name: base_project base_project_building_id_3d003c4c_fk_base_building_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_project
    ADD CONSTRAINT base_project_building_id_3d003c4c_fk_base_building_id FOREIGN KEY (building_id) REFERENCES public.base_building(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3137 (class 2606 OID 42604)
-- Name: base_project base_project_contact_fk_id_c74d5bf0_fk_base_contact_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_project
    ADD CONSTRAINT base_project_contact_fk_id_c74d5bf0_fk_base_contact_id FOREIGN KEY (contact_fk_id) REFERENCES public.base_contact(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3135 (class 2606 OID 42548)
-- Name: base_project base_project_floor_id_766c641a_fk_base_floor_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_project
    ADD CONSTRAINT base_project_floor_id_766c641a_fk_base_floor_id FOREIGN KEY (floor_id) REFERENCES public.base_floor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3136 (class 2606 OID 42553)
-- Name: base_project base_project_macro_schedule_id_fc13f5dd_fk_base_macr; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_project
    ADD CONSTRAINT base_project_macro_schedule_id_fc13f5dd_fk_base_macr FOREIGN KEY (macro_schedule_id) REFERENCES public.base_macroschedule(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3132 (class 2606 OID 42532)
-- Name: base_provider base_provider_material_id_7d6bda5c_fk_base_contact_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_provider
    ADD CONSTRAINT base_provider_material_id_7d6bda5c_fk_base_contact_id FOREIGN KEY (material_id) REFERENCES public.base_contact(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3130 (class 2606 OID 42520)
-- Name: base_state base_state_apartment_id_d0989ff4_fk_base_apartment_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_state
    ADD CONSTRAINT base_state_apartment_id_d0989ff4_fk_base_apartment_id FOREIGN KEY (apartment_id) REFERENCES public.base_apartment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3131 (class 2606 OID 42525)
-- Name: base_state base_state_user_id_58f411f7_fk_base_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_state
    ADD CONSTRAINT base_state_user_id_58f411f7_fk_base_customuser_id FOREIGN KEY (user_id) REFERENCES public.base_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3117 (class 2606 OID 34211)
-- Name: base_subcategory base_subcategory_subcategory_two_id_319517c6_fk_base_subc; Type: FK CONSTRAINT; Schema: public; Owner: lecesse
--

ALTER TABLE ONLY public.base_subcategory
    ADD CONSTRAINT base_subcategory_subcategory_two_id_319517c6_fk_base_subc FOREIGN KEY (subcategory_two_id) REFERENCES public.base_subcategorytwo(id) DEFERRABLE INITIALLY DEFERRED;


-- Completed on 2019-12-27 11:43:17 -05

--
-- PostgreSQL database dump complete
--


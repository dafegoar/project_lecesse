from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from base.choices import ROLE_CHOICES, IT_PROPOSE_CHOICES, TYPE_MATERIAL_CHOICES



class Environment(models.Model):
    name = models.CharField(max_length=100, null=False)
    description = models.CharField(max_length=150, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class SubcategoryTwo(models.Model):
    name = models.CharField(max_length=100, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Subcategory(models.Model):
    name = models.CharField(max_length=100, null=False)
    subcategory_two = models.ForeignKey(SubcategoryTwo, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=100, null=False)
    subcategory = models.ForeignKey(SubcategoryTwo, on_delete=models.CASCADE)
    enviroment = models.ForeignKey(Environment, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class TypeMeasure(models.Model):
    name = models.CharField(max_length=100, null=False)

    def __str__(self):
        return self.name


class Material(models.Model):
    name = models.CharField(max_length=100, null=False)
    description = models.CharField(max_length=200, null=False)
    image = models.ImageField(upload_to='material')
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    subcategory = models.ForeignKey(Subcategory, on_delete=models.CASCADE)
    subcategory_two = models.ForeignKey(SubcategoryTwo, on_delete=models.CASCADE)
    type_measure = models.ForeignKey(TypeMeasure, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)


class MaterialEnvironment(models.Model):
    type_material = models.CharField(max_length=200,choices=TYPE_MATERIAL_CHOICES, null=False)
    material_unity = models.CharField(max_length=100, null=False)
    unity_price = models.IntegerField(null=False)
    area_environment = models.IntegerField(null=False )
    total_price = models.IntegerField(null=True)
    is_proposal = models.CharField(max_length=100, choices=IT_PROPOSE_CHOICES ,null=False)
    environment = models.ForeignKey(Environment, on_delete=models.CASCADE)
    material = models.ForeignKey(Material, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.type_material)


class Contact(models.Model):
    first_name = models.CharField(max_length=100, null=False)
    last_name = models.CharField(max_length=100, null=False)
    position = models.CharField(max_length=100, null=False)
    phone_number = models.CharField(max_length=100, null=False)
    responsible_account = models.CharField(max_length=100, null=False)
    project_name = models.CharField(max_length=100, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.first_name


class Provider(models.Model):
    name = models.CharField(max_length=100, null=False)
    phone_number = models.CharField(max_length=100, null=False)
    email = models.CharField(max_length=100, null=False)
    address_company = models.CharField(max_length=200, null=False)
    activity = models.CharField(max_length=100, null=False)
    state = models.CharField(max_length=100, null=False)
    zip_code = models.CharField(max_length=100, null=False)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name



class Model(models.Model):
    name = models.CharField(max_length=100, null=False)
    image_flat = models.ImageField(upload_to='material')
    environment = models.ForeignKey(Environment, on_delete=models.CASCADE)
    environment_material = models.ForeignKey(MaterialEnvironment, on_delete=models.CASCADE)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Building(models.Model):
    number = models.IntegerField(null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.number)


class Wing(models.Model):
    name = models.CharField(max_length=100, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)


class Floor(models.Model):
    number = models.IntegerField(null=False)
    building = models.ForeignKey(Building, on_delete=models.CASCADE)
    wing =  models.ForeignKey(Wing, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.number)


class MacroSchedule(models.Model):
    name = models.CharField(max_length=100, null=False)
    date_init = models.CharField(max_length=100, null=False)
    date_finish = models.CharField(max_length=100, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)


class Apartment(models.Model):
    number = models.IntegerField(null=False)
    floor = models.ForeignKey(Floor, on_delete=models.CASCADE)
    model = models.ForeignKey(Model, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.number)



class Project(models.Model):
    name = models.CharField(max_length=150, null=False)
    address = models.CharField(max_length=150, null=False)
    city = models.CharField(max_length=100, null=False)
    state = models.CharField(max_length=100, null=False)
    zip_code = models.CharField(max_length=100, null=False)
    owner_project = models.CharField(max_length=100, null=False)
    macro_schedule = models.OneToOneField(MacroSchedule, on_delete=models.CASCADE)
    contact_fk = models.ForeignKey(Contact, on_delete=models.CASCADE)
    building = models.ForeignKey(Building, on_delete=models.CASCADE)
    apartment = models.ForeignKey(Apartment, on_delete=models.CASCADE)
    floor = models.ForeignKey(Floor, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)


class CustomUser(AbstractUser):
    email = models.EmailField(_('email address'), unique=True)
    document_number = models.CharField(max_length=150, null=False)
    phone_number = models.CharField(max_length=30, null=False)
    address = models.CharField(max_length=100, null=False)
    role = models.CharField(max_length=30, choices=ROLE_CHOICES, null=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []


class State(models.Model):
    name = models.CharField(max_length=100, null=False)
    commentary = models.CharField(max_length=200, null=False)
    assessment = models.CharField(max_length=150, null=False)
    percentage = models.CharField(max_length=150, null=False)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    apartment = models.ForeignKey(Apartment, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)

from django.shortcuts import render, redirect
from base.forms import *
from base.models import *


# Create your views here.
from base.forms import FloorForm



def login(request):
    """Show login view"""
    return render(request, 'login.html')


def forgot_password(request):
    """Show Forgot Password View"""
    return render(request, 'forgot_password.html')


def index(request):
    """show index view"""
    return render(request, 'index.html')

# --------------------------------------------------Users------------------------------------------------!!!!!!!!!!!!

def users(request):
    """show users view"""
    users = CustomUser.objects.all()
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = form.save()
            return redirect('/base/users/')
        else:
            return render(request, 'users.html', {'form': form,'users':users})
    else:
        form = CustomUserCreationForm()
        return render(request, 'users.html', {'form': form, 'users':users})


def edit_user(request, id_user):
    """allow edit a user"""
    user = CustomUser.objects.get(pk=id_user)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = CustomUserChangeForm(instance=user)
    else:
        # Aqui guardamos el cliente
        form = CustomUserChangeForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return redirect('/base/users/')
    return render(request, 'edit_user.html', {'form': form})


def view_user(request, id_user):
    """can view data user"""
    user = CustomUser.objects.get(pk=id_user)
    return render(request, 'view_user.html', {'user': user})


def delete_user(request, id_user):
    """delete a user"""
    user = CustomUser.objects.get(pk=id_user)
    user.delete()
    return redirect('/base/users/')

# --------------------------------------------------Environments------------------------------------------------!!!!!!!!!!!!

def enviroments(request):
    """show users view"""
    enviroments = Environment.objects.all()
    if request.method == 'POST':
        form = EnvironmentsForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            enviroment = Environment(
                name=data['name'],
                description=data['description']
            )
            enviroment.save()
            return redirect('/base/enviroments/')
        else:
            return render(request, 'enviroments.html', {'form': form ,'enviroments':enviroments})
    else:
        form = EnvironmentsForm()
        return render(request, 'enviroments.html', {'form': form, 'enviroments':enviroments})


def edit_enviroment(request, id_enviroment):
    """can edit a enviroment"""
    enviroment = Environment.objects.get(pk=id_enviroment)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Environments_Edit_Form(instance=enviroment)
    else:
        # Aqui guardamos el cliente
        form = Environments_Edit_Form(request.POST, instance=enviroment)
        if form.is_valid():
            form.save()
            return redirect('/base/enviroments')
    return render(request, 'edit_enviroment.html', {'form': form})


def view_enviroment(request, id_enviroment):
    """can view data enviroment"""
    enviroment = Environment.objects.get(pk=id_enviroment)
    return render(request, 'view_enviroment.html', {'enviroment': enviroment})


def delete_enviroment(request, id_enviroment):
    """delete a enviroment"""
    enviroment = Environment.objects.get(pk=id_enviroment)
    enviroment.delete()
    return redirect('/base/enviroments')


def category_subcategory(request):
    """show category and subcategory view"""
    categories = Category.objects.all()
    return render(request, 'category_subcategory.html', {'categories': categories})

# --------------------------------------------------Items------------------------------------------------!!!!!!!!!!!!

def create_item(request):
    """show users view"""
    subategoriestwo = SubcategoryTwo.objects.all()
    if request.method == 'POST':
        form = SubcategoryTwoForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            subcategorytwo  = SubcategoryTwo(
                name=data['name'],
            )
            subcategorytwo.save()
            return redirect('/base/create_item/')
        else:
            return render(request, 'subcategorytwo.html', {'form': form ,'subategoriestwo':subategoriestwo})
    else:
        form = SubcategoryTwoForm()
        return render(request, 'subcategorytwo.html', {'form': form, 'subategoriestwo':subategoriestwo})


def view_item(request, id_item):
    """can view data enviroment"""
    subcategorytwo = SubcategoryTwo.objects.get(pk=id_item)
    return render(request, 'view_subcategorytwo.html', {'subcategorytwo': subcategorytwo})


def edit_item(request, id_item):
    """can edit a enviroment"""
    subcategorytwo = SubcategoryTwo.objects.get(pk=id_item)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = SubcategoryTwo_Edit_Form(instance=subcategorytwo)
    else:
        # Aqui guardamos el cliente
        form = SubcategoryTwo_Edit_Form(request.POST, instance=subcategorytwo)
        if form.is_valid():
            form.save()
            return redirect('/base/create_item/')
    return render(request, 'edit_subcategorytwo.html', {'form': form})


def delete_item(request, id_item):
    """delete a enviroment"""
    subcategorytwo = SubcategoryTwo.objects.get(pk=id_item)
    subcategorytwo.delete()
    return redirect('/base/create_item/')

# --------------------------------------------------Subcategory------------------------------------------------!!!!!!!!!!!!

def create_subcategory(request):
    """show users view"""
    subcategories = Subcategory.objects.all()
    if request.method == 'POST':
        form = SubcategoryForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            subcategory  = Subcategory(
                name=data['name'],
                subcategory_two=data['subcategory_two']
            )
            subcategory.save()
            return redirect('/base/create_subcategory/')
        else:
            return render(request, 'subcategory.html', {'form': form ,'subcategories':subcategories})
    else:
        form = SubcategoryForm()
        return render(request, 'subcategory.html', {'form': form, 'subcategories':subcategories})


def view_subcategory(request, id_subcategory):
    """can view data enviroment"""
    subcategory = Subcategory.objects.get(pk=id_subcategory)
    return render(request, 'view_subcategory.html', {'subcategory': subcategory})


def edit_subcategory(request, id_subcategory):
    """can edit a enviroment"""
    subcategory = Subcategory.objects.get(pk=id_subcategory)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Subcategory_Edit_Form(instance=subcategory)
    else:
        # Aqui guardamos el cliente
        form = Subcategory_Edit_Form(request.POST, instance=subcategory)
        if form.is_valid():
            form.save()
            return redirect('/base/create_subcategory/')
    return render(request, 'edit_subcategory.html', {'form': form})


def delete_subcategory(request, id_subcategory):
    """delete a enviroment"""
    subcategory = Subcategory.objects.get(pk=id_subcategory)
    subcategory.delete()
    return redirect('/base/create_subcategory/')


# --------------------------------------------------Categories------------------------------------------------!!!!!!!!!!!!

def create_category(request):
    """can create a category"""
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            category  = Category(
                name=data['name'],
                subcategory=data['subcategory'],
                enviroment=data['enviroment']
            )
            category.save()
            return redirect('/base/category_subcategory/')
        else:
            return render(request, 'category.html', {'form': form })
    else:
        form = CategoryForm()
        return render(request, 'category.html', {'form': form})


def view_category(request, id_category):
    """can view data enviroment"""
    category = Category.objects.get(pk=id_category)
    return render(request, 'view_category.html', {'category': category})


def edit_category(request, id_category):
    """can edit a enviroment"""
    category = Category.objects.get(pk=id_category)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Category_Edit_Form(instance=category)
    else:
        # Aqui guardamos el cliente
        form = Category_Edit_Form(request.POST, instance=category)
        if form.is_valid():
            form.save()
            return redirect('/base/category_subcategory/')
    return render(request, 'edit_category.html', {'form': form})


def delete_category(request, id_category):
    """delete a category"""
    category = Category.objects.get(pk=id_category)
    category.delete()
    return redirect('/base/category_subcategory/')

# --------------------------------------------------Measures------------------------------------------------!!!!!!!!!!!!


def create_type_measure(request):
    """can create a category"""
    types_measure = TypeMeasure.objects.all()
    if request.method == 'POST':
        form = TypeMeasureForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            type_measure  = TypeMeasure(
                name=data['name'],
            )
            type_measure.save()
            return redirect('/base/create_type_measure/')
        else:
            return render(request, 'type_measure.html', {'form': form, 'types_measure':types_measure })
    else:
        form = TypeMeasureForm()
        return render(request, 'type_measure.html', {'form': form, 'types_measure':types_measure})


def view_type_measure(request, id_type_measure):
    """can view data enviroment"""
    type_measure = TypeMeasure.objects.get(pk=id_type_measure)
    return render(request, 'view_type_measure.html', {'type_measure': type_measure})


def edit_type_measure(request, id_type_measure):
    """can edit a enviroment"""
    type_measure = TypeMeasure.objects.get(pk=id_type_measure)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = TypeMeasure_Edit_Form(instance=type_measure)
    else:
        # Aqui guardamos el cliente
        form = TypeMeasure_Edit_Form(request.POST, instance=type_measure)
        if form.is_valid():
            form.save()
            return redirect('/base/create_type_measure/')
    return render(request, 'edit_type_measure.html', {'form': form})


def delete_type_measure(request, id_type_measure):
    """delete a category"""
    type_measure = TypeMeasure.objects.get(pk=id_type_measure)
    type_measure.delete()
    return redirect('/base/create_type_measure/')

# --------------------------------------------------Meterials------------------------------------------------!!!!!!!!!!!!

def show_materials(request):
    """show view materials"""
    materials = MaterialEnvironment.objects.all()
    return render(request, 'materials.html', {'materials': materials})


def create_material(request):
    """can create a category"""
    materials = Material.objects.all()
    if request.method == 'POST':
        form = MaterialForm(request.POST, request.FILES)
        if form.is_valid():
            material = form.save(commit=False)
            material.save()
            return redirect('/base/create_material/')
        else:
            return render(request, 'create_material.html', {'form': form,'materials':materials })
    else:
        form = MaterialForm()
        return render(request, 'create_material.html', {'form': form,'materials':materials})


def view_material(request, id_material):
    """can view data enviroment"""
    material = Material.objects.get(pk=id_material)
    return render(request, 'view_material.html', {'material': material})


def edit_material(request, id_material):
    """can edit a enviroment"""
    material = Material.objects.get(pk=id_material)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Material_Edit_Form(instance=material)
    else:
        # Aqui guardamos el cliente
        form = Material_Edit_Form(request.POST or None, request.FILES or None, instance=material)
        if form.is_valid():
            form.save()
            return redirect('/base/create_material/')
    return render(request, 'edit_material.html', {'form': form})


def delete_material(request, id_material):
    """delete a category"""
    material = Material.objects.get(pk=id_material)
    material.delete()
    return redirect('/base/create_material/')

# --------------------------------------------------Materials Environments------------------------------------------------!!!!!!!!!!!!

def material_environment(request):
    """can create a category"""
    materials_environment = MaterialEnvironment.objects.all()
    if request.method == 'POST':
        form = MaterialEnvironmentForm(request.POST)
        if form.is_valid():
            material_environment = form.save(commit=False)
            material_environment.save()
            return redirect('/base/material_enviroment/')
        else:
            return render(request, 'material_environments.html', {'form': form,'materials_environment':materials_environment })
    else:
        form = MaterialEnvironmentForm()
        return render(request, 'material_environments.html', {'form': form,'materials_environment':materials_environment})


def view_material_environment(request, id_material_environment):
    """can view data enviroment"""
    material_environment = MaterialEnvironment.objects.get(pk=id_material_environment)
    return render(request, 'view_material_environment.html', {'material_environment': material_environment})


def edit_material_environment(request, id_material_environment):
    """can edit a enviroment"""
    material_environment = MaterialEnvironment.objects.get(pk=id_material_environment)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Material_Environment_Edit_Form(instance=material_environment)
    else:
        # Aqui guardamos el cliente
        form = Material_Environment_Edit_Form(request.POST, instance=material_environment)
        if form.is_valid():
            form.save()
            return redirect('/base/material_enviroment/')
    return render(request, 'edit_material_environment.html', {'form': form})


def delete_material_environment(request, id_material_environment):
    """delete a category"""
    material_environment = MaterialEnvironment.objects.get(pk=id_material_environment)
    material_environment.delete()
    return redirect('/base/material_enviroment/')


# --------------------------------------------------Providers------------------------------------------------!!!!!!!!!!!!

def show_providers(request):
    """show view materials"""
    providers = Provider.objects.all()
    return render(request, 'providers.html', {'providers': providers})


def create_provider(request):
    """can create a category"""
    providers = Provider.objects.all()
    if request.method == 'POST':
        form = ProviderForm(request.POST)
        if form.is_valid():
            provider = form.save(commit=False)
            provider.save()
            return redirect('/base/create_provider/')
        else:
            return render(request, 'create_provider.html', {'form': form,'providers':providers })
    else:
        form = ProviderForm()
        return render(request, 'create_provider.html', {'form': form,'providers':providers})


def view_provider(request, id_provider):
    """can view data enviroment"""
    provider = Provider.objects.get(pk=id_provider)
    return render(request, 'view_provider.html', {'provider': provider})


def edit_provider(request, id_provider):
    """can edit a enviroment"""
    provider = Provider.objects.get(pk=id_provider)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Provider_Edit_Form(instance=provider)
    else:
        # Aqui guardamos el cliente
        form = Provider_Edit_Form(request.POST, instance=provider)
        if form.is_valid():
            form.save()
            return redirect('/base/create_provider/')
    return render(request, 'edit_provider.html', {'form': form})


def delete_provider(request, id_provider):
    """delete a category"""
    provider = Provider.objects.get(pk=id_provider)
    provider.delete()
    return redirect('/base/create_provider/')

# --------------------------------------------------Contacts------------------------------------------------!!!!!!!!!!!!

def create_contact(request):
    """can create a category"""
    contacts = Contact.objects.all()
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            contact = form.save(commit=False)
            contact.save()
            return redirect('/base/create_contact/')
        else:
            return render(request, 'contacts.html', {'form': form,'contacts':contacts })
    else:
        form = ContactForm()
        return render(request, 'contacts.html', {'form': form,'contacts':contacts})


def view_contact(request, id_contact):
    """can view data enviroment"""
    contact = Contact.objects.get(pk=id_contact)
    return render(request, 'view_contact.html', {'contact': contact})


def edit_contact(request, id_contact):
    """can edit a enviroment"""
    contact = Contact.objects.get(pk=id_contact)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Contact_Edit_Form(instance=contact)
    else:
        # Aqui guardamos el cliente
        form = Contact_Edit_Form(request.POST, instance=contact)
        if form.is_valid():
            form.save()
            return redirect('/base/create_contact/')
    return render(request, 'edit_contact.html', {'form': form})


def delete_contact(request, id_contact):
    """delete a category"""
    contact = Contact.objects.get(pk=id_contact)
    contact.delete()
    return redirect('/base/create_contact/')


# --------------------------------------------------Models------------------------------------------------!!!!!!!!!!!!

def create_model(request):
    """can create a category"""
    models = Model.objects.all()
    if request.method == 'POST':
        form = ModelForm(request.POST, request.FILES)
        if form.is_valid():
            model = form.save(commit=False)
            model.save()
            return redirect('/base/create_model/')
        else:
            return render(request, 'models.html', {'form': form,'models':models })
    else:
        form = ModelForm()
        return render(request, 'models.html', {'form': form,'models':models})


def view_model(request, id_model):
    """can view data enviroment"""
    model = Model.objects.get(pk=id_model)
    return render(request, 'view_model.html', {'model': model})


def edit_model(request, id_model):
    """can edit a enviroment"""
    model = Model.objects.get(pk=id_model)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Model_Edit_Form(instance=model)
    else:
        # Aqui guardamos el cliente
        form = Model_Edit_Form(request.POST or None, request.FILES or None, request.POST, instance=model)
        if form.is_valid():
            form.save()
            return redirect('/base/create_model/')
    return render(request, 'edit_model.html', {'form': form})


def delete_model(request, id_model):
    """delete a category"""
    model = Model.objects.get(pk=id_model)
    model.delete()
    return redirect('/base/create_model/')

# --------------------------------------------------Projects------------------------------------------------!!!!!!!!!!!!

def show_projects(request):
    """show view materials"""
    projects = Project.objects.all()
    return render(request, 'projects.html', {'projects': projects})


# --------------------------------------------------Wings------------------------------------------------!!!!!!!!!!!!

def create_wing(request):
    """can create a category"""
    wings = Wing.objects.all()
    if request.method == 'POST':
        form = WingForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.save()
            return redirect('/base/create_wing/')
        else:
            return render(request, 'create_wing.html', {'form': form,'wings':wings })
    else:
        form = WingForm()
        return render(request, 'create_wing.html', {'form': form,'wings':wings})


def view_wing(request, id_wing):
    """can view data enviroment"""
    wing = Wing.objects.get(pk=id_wing)
    return render(request, 'view_wing.html', {'wing': wing})


def edit_wing(request, id_wing):
    """can edit a enviroment"""
    wing = Wing.objects.get(pk=id_wing)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Wing_Edit_Form(instance=wing)
    else:
        # Aqui guardamos el cliente
        form = Wing_Edit_Form(request.POST, instance=wing)
        if form.is_valid():
            form.save()
            return redirect('/base/create_wing/')
    return render(request, 'edit_wing.html', {'form': form})


def delete_wing(request, id_wing):
    """delete a category"""
    wing = Wing.objects.get(pk=id_wing)
    wing.delete()
    return redirect('/base/create_wing/')


# --------------------------------------------------Buildings------------------------------------------------!!!!!!!!!!!!

def create_building(request):
    """can create a category"""
    buildings = Building.objects.all()
    if request.method == 'POST':
        form = BuildingForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.save()
            return redirect('/base/create_building/')
        else:
            return render(request, 'create_building.html', {'form': form,'buildings':buildings })
    else:
        form = BuildingForm()
        return render(request, 'create_building.html', {'form': form,'buildings':buildings})


def view_building(request, id_building):
    """can view data enviroment"""
    building = Building.objects.get(pk=id_building)
    return render(request, 'view_building.html', {'building': building})


def edit_building(request, id_building):
    """can edit a enviroment"""
    building = Building.objects.get(pk=id_building)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Building_Edit_Form(instance=building)
    else:
        # Aqui guardamos el cliente
        form = Building_Edit_Form(request.POST, instance=building)
        if form.is_valid():
            form.save()
            return redirect('/base/create_building/')
    return render(request, 'edit_building.html', {'form': form})


def delete_building(request, id_building):
    """delete a category"""
    building = Building.objects.get(pk=id_building)
    building.delete()
    return redirect('/base/create_building/')


# --------------------------------------------------floors------------------------------------------------!!!!!!!!!!!!

def create_floor(request):
    """can create a category"""
    floors = Floor.objects.all()
    if request.method == 'POST':
        form = FloorForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.save()
            return redirect('/base/create_floor/')
        else:
            return render(request, 'create_floor.html', {'form': form,'floors':floors})
    else:
        form = FloorForm()
        return render(request, 'create_floor.html', {'form': form,'floors':floors})


def view_floor(request, id_floor):
    """can view data enviroment"""
    floor = Floor.objects.get(pk=id_floor)
    return render(request, 'view_floor.html', {'floor': floor})


def edit_floor(request, id_floor):
    """can edit a enviroment"""
    floor = Floor.objects.get(pk=id_floor)

    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Floor_Edit_Form(instance=floor)
    else:
        # Aqui guardamos el cliente
        form = Floor_Edit_Form(request.POST, instance=floor)
        if form.is_valid():
            form.save()
            return redirect('/base/create_floor/')
    return render(request, 'edit_floor.html', {'form': form})


def delete_floor(request, id_floor):
    """delete a category"""
    floor = Floor.objects.get(pk=id_floor)
    floor.delete()
    return redirect('/base/create_floor/')


# --------------------------------------------------apartments------------------------------------------------!!!!!!!!!!!!

def create_apartment(request):
    """can create a category"""
    apartments = Apartment.objects.all()
    if request.method == 'POST':
        form = ApartmentForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.save()
            return redirect('/base/create_apartment/')
        else:
            return render(request, 'create_apartment.html', {'form': form,'apartments':apartments})
    else:
        form = ApartmentForm()
        return render(request, 'create_apartment.html', {'form': form,'apartments':apartments})


def view_apartment(request, id_apartment):
    """can view data enviroment"""
    apartment = Apartment.objects.get(pk=id_apartment)
    return render(request, 'view_apartment.html', {'apartment': apartment})


def edit_apartment(request, id_apartment):
    """can edit a enviroment"""
    apartment = Apartment.objects.get(pk=id_apartment)
    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Apartment_Edit_Form(instance=apartment)
    else:
        # Aqui guardamos el cliente
        form = Apartment_Edit_Form(request.POST, instance=apartment)
        if form.is_valid():
            form.save()
            return redirect('/base/create_apartment/')
    return render(request, 'edit_apartment.html', {'form': form})


def delete_apartment(request, id_apartment):
    """delete a category"""
    apartment = Apartment.objects.get(pk=id_apartment)
    apartment.delete()
    return redirect('/base/create_apartment/')

# --------------------------------------------------states------------------------------------------------!!!!!!!!!!!!

def create_state(request):
    """can create a category"""
    states = State.objects.all()
    if request.method == 'POST':
        form = StateForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.save()
            return redirect('/base/create_state/')
        else:
            return render(request, 'create_state.html', {'form': form,'states':states})
    else:
        form = StateForm()
        return render(request, 'create_state.html', {'form': form,'states':states})


def view_state(request, id_state):
    """can view data enviroment"""
    state = State.objects.get(pk=id_state)
    return render(request, 'view_state.html', {'state': state})


def edit_state(request, id_state):
    """can edit a enviroment"""
    state = State.objects.get(pk=id_state)
    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = State_Edit_Form(instance=state)
    else:
        # Aqui guardamos el cliente
        form = State_Edit_Form(request.POST, instance=state)
        if form.is_valid():
            form.save()
            return redirect('/base/create_state/')
    return render(request, 'edit_state.html', {'form': form})


def delete_state(request, id_state):
    """delete a category"""
    state = State.objects.get(pk=id_state)
    state.delete()
    return redirect('/base/create_state/')

# --------------------------------------------------Macro Schedule------------------------------------------------!!!!!!!!!!!!

def create_macro_schedule(request):
    """can create a category"""
    macroschedules = MacroSchedule.objects.all()
    if request.method == 'POST':
        form = MacroScheduleForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.save()
            return redirect('/base/create_state/')
        else:
            return render(request, 'create_macro_schedule.html', {'form': form,'macroschedules':macroschedules})
    else:
        form = MacroScheduleForm()
        return render(request, 'create_macro_schedule.html', {'form': form,'macroschedules':macroschedules})


def view_macro_schedule(request, id_macro_schedule):
    """can view data enviroment"""
    macroschedule = MacroSchedule.objects.get(pk=id_macro_schedule)
    return render(request, 'view_macro_schedule.html', {'macroschedule': macroschedule})


def delete_macro_schedule(request, id_macro_schedule):
    """delete a category"""
    macroschedule = MacroSchedule.objects.get(pk=id_macro_schedule)
    macroschedule.delete()
    return redirect('/base/create_macro_schedule/')


# --------------------------------------------------Projects------------------------------------------------!!!!!!!!!!!!

def create_project(request):
    """can create a category"""
    projects = Project.objects.all()
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.save()
            return redirect('/base/create_project/')
        else:
            return render(request, 'create_project.html', {'form': form,'projects':projects})
    else:
        form = ProjectForm()
        return render(request, 'create_project.html', {'form': form,'projects':projects})


def view_project(request, id_project):
    """can view data enviroment"""
    project = Project.objects.get(pk=id_project)
    return render(request, 'view_project.html', {'project': project})


def edit_project(request, id_project):
    """can edit a enviroment"""
    project = Project.objects.get(pk=id_project)
    if request.method == 'GET':
        # Get se usa para obtener datos y Post para enviar datos
        form = Project_Edit_Form(instance=project)
    else:
        # Aqui guardamos el cliente
        form = Project_Edit_Form(request.POST, instance=project)
        if form.is_valid():
            form.save()
            return redirect('/base/create_project/')
    return render(request, 'edit_project.html', {'form': form})


def delete_project(request, id_project):
    """delete a category"""
    project = Project.objects.get(pk=id_project)
    project.delete()
    return redirect('/base/create_project/')

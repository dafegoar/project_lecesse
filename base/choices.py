ROLE_CHOICES = (
    ('SALES_REP', 'Sales Rep'),
    ('CONTACTOR', 'Contactor'),
    ('ADMINISTRATOR', 'Administrator'),
    ('CONSTRUCTOR_MANAGER', 'Constructor Manager'),
)
TYPE_MATERIAL_CHOICES = (
    ('STANDARD', 'Standard'),
    ('CUSTOMIZABLE', 'Customizable'),
)
IT_PROPOSE_CHOICES = (
    ('YES', 'Yes'),
    ('NO', 'No'),
)
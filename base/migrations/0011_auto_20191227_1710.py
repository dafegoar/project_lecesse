# Generated by Django 2.2 on 2019-12-27 17:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0010_auto_20191226_1842'),
    ]

    operations = [
        migrations.RenameField(
            model_name='provider',
            old_name='material',
            new_name='contact',
        ),
        migrations.AlterField(
            model_name='materialenvironment',
            name='is_proposal',
            field=models.CharField(choices=[('YES', 'Yes'), ('NO', 'No')], max_length=100),
        ),
        migrations.AlterField(
            model_name='materialenvironment',
            name='type_material',
            field=models.CharField(choices=[('STANDARD', 'Standard'), ('CUSTOMIZABLE', 'Customizable')], max_length=200),
        ),
    ]

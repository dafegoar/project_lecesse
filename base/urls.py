from django.conf.urls import url

from . import views

app_name = 'base'

urlpatterns = [
    #principal view
    url('index/', views.index, name='index'),

    #Forgot Password View
    url('forgot-password/', views.forgot_password, name='forgot_password'),

    #module users
    url('users/', views.users, name='users'),
    url('edit-user/(?P<id_user>[0-9]+)/$', views.edit_user, name='edit_user'),
    url('delete-user/(?P<id_user>[0-9]+)/$', views.delete_user, name='delete_user'),
    url('view-user/(?P<id_user>[0-9]+)/$', views.view_user, name='view_user'),

    #module enviroments
    url('enviroments/', views.enviroments, name='enviroments'),
    url('edit-enviroment/(?P<id_enviroment>[0-9]+)/$', views.edit_enviroment, name='edit_enviroment'),
    url('delete-enviroment/(?P<id_enviroment>[0-9]+)/$', views.delete_enviroment, name='delete_enviroment'),
    url('view-enviroment/(?P<id_enviroment>[0-9]+)/$', views.view_enviroment, name='view_enviroment'),

    #module category and subcategory
    url('category_subcategory/', views.category_subcategory, name='category_subcategory'),
    url('edit-enviroment/(?P<id_enviroment>[0-9]+)/$', views.edit_enviroment, name='edit_enviroment'),
    url('delete-enviroment/(?P<id_enviroment>[0-9]+)/$', views.delete_enviroment, name='delete_enviroment'),
    url('view-enviroment/(?P<id_enviroment>[0-9]+)/$', views.view_enviroment, name='view_enviroment'),

    #module subcategorytwo
    url('create_item/', views.create_item, name='create_item'),
    url('edit-item/(?P<id_item>[0-9]+)/$', views.edit_item, name='edit_item'),
    url('delete-item/(?P<id_item>[0-9]+)/$', views.delete_item, name='delete_item'),
    url('view-item/(?P<id_item>[0-9]+)/$', views.view_item, name='view_item'),

    #module subcategory
    url('create_subcategory/', views.create_subcategory, name='create_subcategory'),
    url('edit-subcategory/(?P<id_subcategory>[0-9]+)/$', views.edit_subcategory, name='edit_subcategory'),
    url('delete-subcategory/(?P<id_subcategory>[0-9]+)/$', views.delete_subcategory, name='delete_subcategory'),
    url('view_subcategory/(?P<id_subcategory>[0-9]+)/$', views.view_subcategory, name='view_subcategory'),

    #module category
    url('create_category/', views.create_category, name='create_category'),
    url('edit-category/(?P<id_category>[0-9]+)/$', views.edit_category, name='edit_category'),
    url('delete-category/(?P<id_category>[0-9]+)/$', views.delete_category, name='delete_category'),
    url('view_category/(?P<id_category>[0-9]+)/$', views.view_category, name='view_category'),

    #module type_measure
    url('create_type_measure/', views.create_type_measure, name='create_type_measure'),
    url('edit-typemeasure/(?P<id_type_measure>[0-9]+)/$', views.edit_type_measure, name='edit_type_measure'),
    url('delete-typemeasure/(?P<id_type_measure>[0-9]+)/$', views.delete_type_measure, name='delete_type_measure'),
    url('view_type_measure/(?P<id_type_measure>[0-9]+)/$', views.view_type_measure, name='view_type_measure'),

    # module material
    url('materials/', views.show_materials, name='show_materials'),
    url('create_material/', views.create_material, name='create_material'),
    url('edit-material/(?P<id_material>[0-9]+)/$', views.edit_material, name='edit_material'),
    url('delete-material/(?P<id_material>[0-9]+)/$', views.delete_material, name='delete_material'),
    url('view_material/(?P<id_material>[0-9]+)/$', views.view_material, name='view_material'),

    # module material enviroments
    url('material_enviroment/', views.material_environment, name='material_environment'),
    url('edit-material_environment/(?P<id_material_environment>[0-9]+)/$', views.edit_material_environment, name='edit_material_environment'),
    url('delete-material_environment/(?P<id_material_environment>[0-9]+)/$', views.delete_material_environment, name='delete_material_environment'),
    url('view_material_environment/(?P<id_material_environment>[0-9]+)/$', views.view_material_environment, name='view_material_environment'),

    # module contact
    url('create_contact/', views.create_contact, name='create_contact'),
    url('edit-contact/(?P<id_contact>[0-9]+)/$', views.edit_contact, name='edit_contact'),
    url('delete-contact/(?P<id_contact>[0-9]+)/$', views.delete_contact, name='delete_contact'),
    url('view_contact/(?P<id_contact>[0-9]+)/$', views.view_contact, name='view_contact'),

    #module providers
    url('providers/', views.show_providers, name='show_providers'),
    url('create_provider/', views.create_provider, name='create_provider'),
    url('edit-provider/(?P<id_provider>[0-9]+)/$', views.edit_provider, name='edit_provider'),
    url('delete-provider/(?P<id_provider>[0-9]+)/$', views.delete_provider, name='delete_provider'),
    url('view_provider/(?P<id_provider>[0-9]+)/$', views.view_provider, name='view_provider'),

    #module models
    url('create_model/', views.create_model, name='create_model'),
    url('edit-model/(?P<id_model>[0-9]+)/$', views.edit_model, name='edit_model'),
    url('delete-model/(?P<id_model>[0-9]+)/$', views.delete_model, name='delete_model'),
    url('view_model/(?P<id_model>[0-9]+)/$', views.view_model, name='view_model'),

    #module projects
    url('projects/', views.show_projects, name='show_projects'),


    #module wing
    url('create_wing/', views.create_wing, name='create_wing'),
    url('edit-wing/(?P<id_wing>[0-9]+)/$', views.edit_wing, name='edit_wing'),
    url('delete-wing/(?P<id_wing>[0-9]+)/$', views.delete_wing, name='delete_wing'),
    url('view_wing/(?P<id_wing>[0-9]+)/$', views.view_wing, name='view_wing'),

    #module building
    url('create_building/', views.create_building, name='create_building'),
    url('edit-building/(?P<id_building>[0-9]+)/$', views.edit_building, name='edit_building'),
    url('delete-building/(?P<id_building>[0-9]+)/$', views.delete_building, name='delete_building'),
    url('view_building/(?P<id_building>[0-9]+)/$', views.view_building, name='view_building'),

    #module floor
    url('create_floor/', views.create_floor, name='create_floor'),
    url('edit-floor/(?P<id_floor>[0-9]+)/$', views.edit_floor, name='edit_floor'),
    url('delete-floor/(?P<id_floor>[0-9]+)/$', views.delete_floor, name='delete_floor'),
    url('view_floor/(?P<id_floor>[0-9]+)/$', views.view_floor, name='view_floor'),

    # module apartment
    url('create_apartment/', views.create_apartment, name='create_apartment'),
    url('edit-apartment/(?P<id_apartment>[0-9]+)/$', views.edit_apartment, name='edit_apartment'),
    url('delete-apartment/(?P<id_apartment>[0-9]+)/$', views.delete_apartment, name='delete_apartment'),
    url('view_apartment/(?P<id_apartment>[0-9]+)/$', views.view_apartment, name='view_apartment'),

    # module state
    url('create_state/', views.create_state, name='create_state'),
    url('edit_state/(?P<id_state>[0-9]+)/$', views.edit_state, name='edit_state'),
    url('delete-state/(?P<id_state>[0-9]+)/$', views.delete_state, name='delete_state'),
    url('view_state/(?P<id_state>[0-9]+)/$', views.view_state, name='view_state'),

    # module macro schedule
    url('create_macro_schedule/', views.create_macro_schedule, name='create_macro_schedule'),
    url('delete-macro_schedule/(?P<id_macro_schedule>[0-9]+)/$', views.delete_macro_schedule, name='delete_macro_schedule'),
    url('view_macro_schedule/(?P<id_macro_schedule>[0-9]+)/$', views.view_macro_schedule, name='view_macro_schedule'),

    # module project
    url('create_project/', views.create_project, name='create_project'),
    url('edit-project/(?P<id_project>[0-9]+)/$', views.edit_project, name='edit_project'),
    url('delete-project/(?P<id_project>[0-9]+)/$', views.delete_project, name='delete_project'),
    url('view_project/(?P<id_project>[0-9]+)/$', views.view_project, name='view_project'),

]

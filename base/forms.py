from django import forms
from .models import *
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = CustomUser
        exclude = ['id','last_login','username' ,'is_staff', 'is_superuser','user_permissions','is_active','date_joined']
        fields = ['first_name', 'last_name' ,'document_number','email', 'phone_number', 'address','password1', 'role', 'project' ]
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'First Name'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Last Name'}),
            'document_number': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Document Number'}),
            'phone_number': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Phone Number'}),
            'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'}),
            'address': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Address'}),
            'role': forms.Select(attrs={'class': 'form-control', 'placeholder': 'Role'}),
            'project': forms.Select(attrs={'class': 'form-control', 'placeholder': 'Project'}),
            'password1': forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}),
        }
        labels = {
            'first_name': ('First Name'),
            'last_name': ('Last Name'),
            'document_number': ('Number Document'),
            'phone_number': ('Number Phone'),
            'email': ('E-Mail'),
            'address': ('Address'),
            'role': ('Role'),
            'project': ('Project'),
            'password1': ('Password'),
        }
        help_texts = {
            'username': '',
            'is_active': '',
            'is_staff': '',
            'groups': '',
        }
    def clean_email(self):
        data = self.cleaned_data['email']
        return data.lower()


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        exclude = ['id', 'last_login', 'username', 'is_staff', 'is_superuser', 'user_permissions', 'is_active',
                   'date_joined']
        fields = ['first_name', 'last_name', 'document_number', 'email', 'phone_number', 'address', 'role',
                  'project']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'document_number': forms.NumberInput(attrs={'class': 'form-control'}),
            'phone_number': forms.NumberInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'role': forms.Select(attrs={'class': 'form-control'}),
            'project': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'first_name': ('First Name'),
            'last_name': ('Last Name'),
            'document_number': ('Number Document'),
            'phone_number': ('Number Phone'),
            'email': ('E-Mail'),
            'address': ('Address'),
            'role': ('Role'),
            'project': ('Project'),
        }
        help_texts = {
            'username': '',
            'is_active': '',
            'is_staff': '',
            'groups': '',
        }

    def clean_email(self):
        data = self.cleaned_data['email']
        return data.lower()


class EnvironmentsForm(forms.ModelForm):
    class Meta:
        model = Environment
        fields = ['name', 'description']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.TextInput(attrs={'class': 'form-control'})
        }
        labels = {
            'name': ('Name '),
            'description': ('Description'),
        }


class Environments_Edit_Form(forms.ModelForm):
    class Meta:
        model = Environment
        fields = ['name', 'description']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.TextInput(attrs={'class': 'form-control'})
        }
        labels = {
            'name': ('Name '),
            'description': ('Description'),
        }


class SubcategoryTwoForm(forms.ModelForm):
    class Meta:
        model = SubcategoryTwo
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name '),
        }


class SubcategoryTwo_Edit_Form(forms.ModelForm):
    class Meta:
        model = SubcategoryTwo
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name '),
        }


class SubcategoryForm(forms.ModelForm):
    class Meta:
        model = Subcategory
        fields = ['name', 'subcategory_two']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'subcategory_two': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name '),
            'subcategory_two': ('Item'),
        }


class Subcategory_Edit_Form(forms.ModelForm):
    class Meta:
        model = Subcategory
        fields = ['name', 'subcategory_two']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'subcategory_two': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name '),
            'subcategory_two': ('Item'),
        }


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name', 'subcategory', 'enviroment']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'subcategory': forms.Select(attrs={'class': 'form-control'}),
            'enviroment': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name '),
            'subcategory': ('Subcategory'),
            'enviroment': ('Enviroment'),
        }


class Category_Edit_Form(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name', 'subcategory', 'enviroment']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'subcategory': forms.Select(attrs={'class': 'form-control'}),
            'enviroment': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name '),
            'subcategory': ('Subcategory'),
            'enviroment': ('Enviroment'),
        }


class MaterialForm(forms.ModelForm):
    class Meta:
        model = Material
        fields = ['name', 'description', 'image', 'category', 'subcategory', 'subcategory_two', 'type_measure']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.TextInput(attrs={'class': 'form-control'}),
            'category': forms.Select(attrs={'class': 'form-control'}),
            'subcategory': forms.Select(attrs={'class': 'form-control'}),
            'subcategory_two': forms.Select(attrs={'class': 'form-control'}),
            'type_measure': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name '),
            'description': ('Description'),
            'image': ('Image'),
            'category': ('Category '),
            'subcategory': ('Subcategory'),
            'subcategory_two': ('Subcategory Two'),
            'type_measure': ('Type Measure'),
        }


class Material_Edit_Form(forms.ModelForm):
    class Meta:
        model = Material
        fields = ['name', 'description', 'image', 'category', 'subcategory', 'subcategory_two', 'type_measure']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.TextInput(attrs={'class': 'form-control'}),
            'category': forms.Select(attrs={'class': 'form-control'}),
            'subcategory': forms.Select(attrs={'class': 'form-control'}),
            'subcategory_two': forms.Select(attrs={'class': 'form-control'}),
            'type_measure': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name '),
            'description': ('Description'),
            'image': ('Image'),
            'category': ('Category '),
            'subcategory': ('Subcategory'),
            'subcategory_two': ('Subcategory Two'),
            'type_measure': ('Type Measure'),
        }


class TypeMeasureForm(forms.ModelForm):
    class Meta:
        model = TypeMeasure
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'})
        }
        labels = {
            'name': ('Name '),
        }


class TypeMeasure_Edit_Form(forms.ModelForm):
    class Meta:
        model = TypeMeasure
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'})
        }
        labels = {
            'name': ('Name ')
        }


class MaterialEnvironmentForm(forms.ModelForm):
    class Meta:
        model = MaterialEnvironment
        fields = ['type_material', 'material_unity', 'unity_price', 'area_environment', 'total_price', 'is_proposal',
                  'environment', 'material']
        widgets = {
            'type_material': forms.Select(attrs={'class': 'form-control'}),
            'material_unity': forms.TextInput(attrs={'class': 'form-control'}),
            'unity_price': forms.NumberInput(attrs={'class': 'form-control'}),
            'area_environment': forms.NumberInput(attrs={'class': 'form-control'}),
            'total_price': forms.NumberInput(attrs={'class': 'form-control'}),
            'is_proposal': forms.Select(attrs={'class': 'form-control'}),
            'environment': forms.Select(attrs={'class': 'form-control'}),
            'material': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'type_material': ('Material Type'),
            'material_unity': ('Material Unity '),
            'unity_price': ('Price Unity'),
            'area_environment': ('Area Environment'),
            'total_price': ('Total Price'),
            'is_proposal': ('Is Proposal'),
            'environment': ('Environment'),
            'material': ('Material'),
        }


class Material_Environment_Edit_Form(forms.ModelForm):
    class Meta:
        model = MaterialEnvironment
        fields = ['type_material', 'material_unity', 'unity_price', 'area_environment', 'total_price', 'is_proposal',
                  'environment', 'material']
        widgets = {
            'type_material': forms.Select(attrs={'class': 'form-control'}),
            'material_unity': forms.TextInput(attrs={'class': 'form-control'}),
            'unity_price': forms.NumberInput(attrs={'class': 'form-control'}),
            'area_environment': forms.NumberInput(attrs={'class': 'form-control'}),
            'total_price': forms.NumberInput(attrs={'class': 'form-control'}),
            'is_proposal': forms.Select(attrs={'class': 'form-control'}),
            'environment': forms.Select(attrs={'class': 'form-control'}),
            'material': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'type_material': ('Material Type'),
            'material_unity': ('Material Unity '),
            'unity_price': ('Price Unity'),
            'area_environment': ('Area Environment'),
            'total_price': ('Total Price'),
            'is_proposal': ('Is Proposal'),
            'environment': ('Environment'),
            'material': ('Material'),
        }

#--------------------------------------------------------------------------------------------------------------------------

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['first_name', 'last_name', 'position', 'phone_number', 'responsible_account', 'project_name']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'position': forms.TextInput(attrs={'class': 'form-control'}),
            'phone_number': forms.NumberInput(attrs={'class': 'form-control'}),
            'responsible_account': forms.TextInput(attrs={'class': 'form-control'}),
            'project_name': forms.TextInput(attrs={'class': 'form-control'}),

        }
        labels = {
            'first_name': ('Fist Name'),
            'last_name': ('Last Name '),
            'position': ('Position'),
            'phone_number': ('Phone Number'),
            'responsible_account': ('Responsible Account'),
            'project_name': ('Project Name'),
        }


class Contact_Edit_Form(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['first_name', 'last_name', 'position', 'phone_number', 'responsible_account', 'project_name']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'position': forms.TextInput(attrs={'class': 'form-control'}),
            'phone_number': forms.NumberInput(attrs={'class': 'form-control'}),
            'responsible_account': forms.TextInput(attrs={'class': 'form-control'}),
            'project_name': forms.TextInput(attrs={'class': 'form-control'}),

        }
        labels = {
            'first_name': ('Fist Name'),
            'last_name': ('Last Name '),
            'position': ('Position'),
            'phone_number': ('Phone Number'),
            'responsible_account': ('Responsible Account'),
            'project_name': ('Project Name'),
        }

#--------------------------------------------------------------------------------------------------------------------------

class ProviderForm(forms.ModelForm):
    class Meta:
        model = Provider
        fields = ['name', 'phone_number', 'email', 'address_company', 'activity', 'state', 'zip_code', 'contact']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'phone_number': forms.NumberInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'address_company': forms.TextInput(attrs={'class': 'form-control'}),
            'activity': forms.TextInput(attrs={'class': 'form-control'}),
            'state': forms.TextInput(attrs={'class': 'form-control'}),
            'zip_code': forms.TextInput(attrs={'class': 'form-control'}),
            'contact': forms.Select(attrs={'class': 'form-control'}),

        }
        labels = {
            'name': ('Name'),
            'phone_number': ('Phone Number '),
            'email': ('Email'),
            'address_company': ('Address Company'),
            'activity': ('Activity'),
            'state': ('State'),
            'zip_code': ('Zip Code'),
            'contact': ('Contact'),
        }


class Provider_Edit_Form(forms.ModelForm):
    class Meta:
        model = Provider
        fields = ['name', 'phone_number', 'email', 'address_company', 'activity', 'state', 'zip_code', 'contact']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'phone_number': forms.NumberInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'address_company': forms.TextInput(attrs={'class': 'form-control'}),
            'activity': forms.TextInput(attrs={'class': 'form-control'}),
            'state': forms.TextInput(attrs={'class': 'form-control'}),
            'zip_code': forms.TextInput(attrs={'class': 'form-control'}),
            'contact': forms.Select(attrs={'class': 'form-control'}),

        }
        labels = {
            'name': ('Name'),
            'phone_number': ('Phone Number '),
            'email': ('Email'),
            'address_company': ('Address Company'),
            'activity': ('Activity'),
            'state': ('State'),
            'zip_code': ('Zip Code'),
            'contact': ('Contact'),
        }


#--------------------------------------------------------------------------------------------------------------------------

class ModelForm(forms.ModelForm):
    class Meta:
        model = Model
        fields = ['name', 'image_flat', 'environment', 'environment_material', 'provider']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'environment': forms.Select(attrs={'class': 'form-control'}),
            'environment_material': forms.Select(attrs={'class': 'form-control'}),
            'provider': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name'),
            'image_flat': ('Image Flat'),
            'environment': ('Environment'),
            'environment_material': ('Environment Material'),
            'provider': ('Provider'),
        }


class Model_Edit_Form(forms.ModelForm):
    class Meta:
        model = Model
        fields = ['name', 'image_flat', 'environment', 'environment_material', 'provider']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'environment': forms.Select(attrs={'class': 'form-control'}),
            'environment_material': forms.Select(attrs={'class': 'form-control'}),
            'provider': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name'),
            'image_flat': ('Image Flat'),
            'environment': ('Environment'),
            'environment_material': ('Environment Material'),
            'provider': ('Provider'),
        }

#--------------------------------------------------------------------------------------------------------------------------

class WingForm(forms.ModelForm):
    class Meta:
        model = Wing
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name'),
        }


class Wing_Edit_Form(forms.ModelForm):
    class Meta:
        model = Wing
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name'),
        }


#--------------------------------------------------------------------------------------------------------------------------

class FloorForm(forms.ModelForm):
    class Meta:
        model = Floor
        fields = ['number', 'building', 'wing']
        widgets = {
            'number': forms.NumberInput(attrs={'class': 'form-control'}),
            'building': forms.Select(attrs={'class': 'form-control'}),
            'wing': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'number': ('Number'),
            'building': ('Building'),
            'wing': ('Wing'),
        }


class Floor_Edit_Form(forms.ModelForm):
    class Meta:
        model = Floor
        fields = ['number', 'building', 'wing']
        widgets = {
            'number': forms.NumberInput(attrs={'class': 'form-control'}),
            'building': forms.Select(attrs={'class': 'form-control'}),
            'wing': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'number': ('Number'),
            'building': ('Building'),
            'wing': ('Wing'),
        }


#--------------------------------------------------------------------------------------------------------------------------

class ApartmentForm(forms.ModelForm):
    class Meta:
        model = Apartment
        fields = ['number', 'floor', 'model']
        widgets = {
            'number': forms.NumberInput(attrs={'class': 'form-control'}),
            'floor': forms.Select(attrs={'class': 'form-control'}),
            'model': forms.Select(attrs={'class': 'form-control'}),
        }

        labels = {
            'number': ('Number'),
            'floor': ('Floor'),
            'model': ('Model'),
        }


class Apartment_Edit_Form(forms.ModelForm):
    class Meta:
        model = Apartment
        fields = ['number', 'floor', 'model']
        widgets = {
            'number': forms.NumberInput(attrs={'class': 'form-control'}),
            'floor': forms.Select(attrs={'class': 'form-control'}),
            'model': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'number': ('Number'),
            'floor': ('Floor'),
            'model': ('Model'),
        }


#--------------------------------------------------------------------------------------------------------------------------

class StateForm(forms.ModelForm):
    class Meta:
        model = State
        fields = ['name', 'commentary', 'assessment', 'percentage', 'user', 'apartment']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'commentary': forms.TextInput(attrs={'class': 'form-control'}),
            'assessment': forms.TextInput(attrs={'class': 'form-control'}),
            'percentage': forms.TextInput(attrs={'class': 'form-control'}),
            'user': forms.Select(attrs={'class': 'form-control'}),
            'apartment': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name'),
            'commentary': ('Commentary'),
            'assessment': ('Assessment'),
            'percentage': ('Percentage'),
            'user': ('User'),
            'apartment': ('Apartment'),

        }


class State_Edit_Form(forms.ModelForm):
    class Meta:
        model = State
        fields = ['name', 'commentary', 'assessment', 'percentage', 'user', 'apartment']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'commentary': forms.TextInput(attrs={'class': 'form-control'}),
            'assessment': forms.TextInput(attrs={'class': 'form-control'}),
            'percentage': forms.TextInput(attrs={'class': 'form-control'}),
            'user': forms.Select(attrs={'class': 'form-control'}),
            'apartment': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name'),
            'commentary': ('Commentary'),
            'assessment': ('Assessment'),
            'percentage': ('Percentage'),
            'user': ('User'),
            'apartment': ('Apartment'),

        }


#--------------------------------------------------------------------------------------------------------------------------

class MacroScheduleForm(forms.ModelForm):
    class Meta:
        model = MacroSchedule
        fields = ['name', 'date_init' , 'date_finish']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'date_init': forms.TextInput(attrs={'class': 'form-control'}),
            'date_finish': forms.TextInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name'),
            'date_init': ('Date Init DD/MM/YYYY'),
            'date_finish': ('Date Finish DD/MM/YYYY'),
        }


class MacroSchedule_Edit_Form(forms.ModelForm):
    class Meta:
        model = MacroSchedule
        fields = ['name', 'date_init', 'date_finish']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'date_init': forms.TextInput(attrs={'class': 'form-control'}),
            'date_finish': forms.TextInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name'),
            'date_init': ('Date Init DD/MM/YYYY'),
            'date_finish': ('Date Finish DD/MM/YYYY'),
        }


#--------------------------------------------------------------------------------------------------------------------------

class BuildingForm(forms.ModelForm):
    class Meta:
        model = Building
        fields = ['number']
        widgets = {
            'number': forms.NumberInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'number': ('Number'),
        }


class Building_Edit_Form(forms.ModelForm):
    class Meta:
        model = Building
        fields = ['number']
        widgets = {
            'number': forms.NumberInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'number': ('Number'),
        }


#--------------------------------------------------------------------------------------------------------------------------

class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['name','address','city','state','zip_code',
                  'owner_project','macro_schedule','contact_fk',
                  'building','apartment','floor']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'city': forms.TextInput(attrs={'class': 'form-control'}),
            'state': forms.TextInput(attrs={'class': 'form-control'}),
            'zip_code': forms.TextInput(attrs={'class': 'form-control'}),
            'owner_project': forms.TextInput(attrs={'class': 'form-control'}),
            'macro_schedule': forms.Select(attrs={'class': 'form-control'}),
            'contact_fk': forms.Select(attrs={'class': 'form-control'}),
            'building': forms.Select(attrs={'class': 'form-control'}),
            'apartment': forms.Select(attrs={'class': 'form-control'}),
            'floor': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name'),
            'address': ('Address'),
            'city': ('City'),
            'state': ('State'),
            'zip_code': ('Zip Code'),
            'owner_project': ('Owner Project'),
            'macro_schedule': ('Macro Schedule'),
            'contact_fk': ('Contact'),
            'building': ('Building'),
            'apartment': ('Apartment'),
            'floor': ('Floor'),

        }


class Project_Edit_Form(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['name', 'address', 'city', 'state', 'zip_code',
                  'owner_project', 'macro_schedule', 'contact_fk',
                  'building', 'apartment', 'floor']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'city': forms.TextInput(attrs={'class': 'form-control'}),
            'state': forms.TextInput(attrs={'class': 'form-control'}),
            'zip_code': forms.TextInput(attrs={'class': 'form-control'}),
            'owner_project': forms.TextInput(attrs={'class': 'form-control'}),
            'macro_schedule': forms.Select(attrs={'class': 'form-control'}),
            'contact_fk': forms.Select(attrs={'class': 'form-control'}),
            'building': forms.Select(attrs={'class': 'form-control'}),
            'apartment': forms.Select(attrs={'class': 'form-control'}),
            'floor': forms.Select(attrs={'class': 'form-control'}),
        }
        labels = {
            'name': ('Name'),
            'address': ('Address'),
            'city': ('City'),
            'state': ('State'),
            'zip_code': ('Zip Code'),
            'owner_project': ('Owner Project'),
            'macro_schedule': ('Macro Schedule'),
            'contact_fk': ('Contact'),
            'building': ('Building'),
            'apartment': ('Apartment'),
            'floor': ('Floor'),

        }
